# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if Point.count == 0
  Point.create([
                 # { name: 'championship_winner', value: 190, round_factor: true },
                 # { name: 'championship_top_score', value: 190, round_factor: true },
                 {name: "scoreboard", value: 70},
                 {name: "winner", value: 30},
                 {name: "team_fouls", value: 10},
                 {name: "team_yellow_cards", value: 10},
                 {name: "all_goals", value: 5},
                 {name: "all_fouls", value: 3},
                 {name: "all_yellow_cards", value: 3},
                 {name: "team_goals_per_period", value: 2},
                 {name: "ball_possession", value: 1},
               ])
end

if League.count == 0
  League.create([
                  {tournament_code: "sr:tournament:325", name: "Brasileirão", continent: "am"},
                  {tournament_code: "sr:tournament:384", name: "Copa Libertadores", continent: "am"},
                  {tournament_code: "sr:tournament:7", name: "Champions League", continent: "eu"},
                  {tournament_code: "sr:tournament:8", name: "Campeonato Espanhol", continent: "eu"},
                  {tournament_code: "sr:tournament:17", name: "Premier League", continent: "eu"},
                ])
end

if Season.count == 0
  Season.create([
                  {league_id: 1, season_code: "sr:season:51253", start_date: "2018-04-14", end_date: "2018-12-16", matches: 383, active: true},
                  {league_id: 2, season_code: "sr:season:50175", start_date: "2018-01-22", end_date: "2018-12-01", matches: 156, active: true},
                  {league_id: 3, season_code: "sr:season:54533", start_date: "2018-06-26", end_date: "2019-06-02", matches: 188, active: true},
                  {league_id: 4, season_code: "sr:season:55917", start_date: "2018-08-17", end_date: "2019-05-20", matches: 382, active: true},
                  {league_id: 5, season_code: "sr:season:54571", start_date: "2018-08-10", end_date: "2019-05-13", matches: 380, active: true},
                ])
end

if User.count == 0
  1.upto(777) do |_i|
    password = FFaker::Internet.password(8)

    User.create(name: FFaker::NameBR.unique.name,
                email: FFaker::Internet.unique.email,
                encrypted_password: BCrypt::Password.create(password),
                password: password)
  end
end

if Hint.count == 0 && Match.count > 0
  1.upto(777 * 3) do |_i|
    Hint.create(match: Match.all.order("RAND()").limit(1).first,
                user: User.where("id < 777").order("RAND()").limit(1).first)
  end
end

if Hint.count > 0 && UserPoint.count == 0
  Hint.find_each do |hint|
    UserPoint.create(hint: hint,
                     user: hint.user,
                     points: (rand() * 100).to_i)
  end
end

if Reward.count == 0
  Reward.create(position: 1, award: 10)
end
