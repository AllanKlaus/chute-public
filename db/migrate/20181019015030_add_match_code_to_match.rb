class AddMatchCodeToMatch < ActiveRecord::Migration[5.2]
  def change
    add_column :matches, :match_code, :string, null: false
  end
end
