class AddMoneyFromUserDatum < ActiveRecord::Migration[5.2]
  def change
    add_column :user_data, :money, :integer, default: 0
  end
end
