class RemoveExternalIdFromMatch < ActiveRecord::Migration[5.2]
  def change
    remove_column :matches, :external_id, :integer
  end
end
