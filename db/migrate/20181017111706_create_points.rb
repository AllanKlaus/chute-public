class CreatePoints < ActiveRecord::Migration[5.2]
  def change
    create_table :points do |t|
      t.string :name
      t.integer :value
      t.boolean :round_factor, default: false
    end
  end
end
