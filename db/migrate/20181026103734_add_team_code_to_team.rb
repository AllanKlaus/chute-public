class AddTeamCodeToTeam < ActiveRecord::Migration[5.2]
  def change
    add_column :teams, :team_code, :string, index: true, null: false
  end
end
