class CreateSeasons < ActiveRecord::Migration[5.2]
  def change
    create_table :seasons do |t|
      t.references :league, foreign_key: true, index: true
      t.string :season_code, null: false
      t.date :start_date
      t.date :end_date
      t.integer :year, limit: 4
      t.integer :matches, limit: 3

      t.timestamps
    end
  end
end
