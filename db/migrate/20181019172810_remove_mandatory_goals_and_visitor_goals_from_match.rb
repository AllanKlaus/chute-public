class RemoveMandatoryGoalsAndVisitorGoalsFromMatch < ActiveRecord::Migration[5.2]
  def change
    remove_column :matches, :mandatory_goals, :integer
    remove_column :matches, :visitor_goals, :integer
  end
end
