class CreateRewards < ActiveRecord::Migration[5.2]
  def change
    create_table :rewards do |t|
      t.integer :position
      t.decimal :award, precision: 10, scale: 2

      t.timestamps
    end
  end
end
