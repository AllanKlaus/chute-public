class CreateMoviments < ActiveRecord::Migration[5.2]
  def change
    create_table :moviments do |t|
      t.references :user, foreign_key: true
      t.decimal :money, precision: 10, scale: 2, default: 0
      t.integer :type, limit: 1

      t.timestamps
    end
  end
end
