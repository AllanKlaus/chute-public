class RemoveMoneyFromMoviment < ActiveRecord::Migration[5.2]
  def change
    remove_column :moviments, :money, :decimal
  end
end
