class CreateMatches < ActiveRecord::Migration[5.2]
  def change
    create_table :matches do |t|
      t.integer :external_id
      t.references :mandatory, references: :team, index: true
      t.references :visitor, references: :team, index: true
      t.date :match_date
      t.integer :mandatory_goals, limit: 2
      t.integer :visitor_goals, limit: 2

      t.timestamps
    end
  end
end
