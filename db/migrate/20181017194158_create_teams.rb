class CreateTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :teams do |t|
      t.string :name, null: false
      t.string :abbreviation, null: false, limit: 3
      t.string :shield

      t.timestamps
    end
  end
end
