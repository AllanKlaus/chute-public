class AddMoneyFromMoviment < ActiveRecord::Migration[5.2]
  def change
    add_column :moviments, :money, :integer, default: 0
  end
end
