class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :subject
      t.string :email
      t.boolean :read, default: true
      t.references :user, foreign_key: true, default: nil

      t.timestamps
    end
  end
end
