class AddContinentToLeague < ActiveRecord::Migration[5.2]
  def change
    add_column :leagues, :continent, :string, limit: 2
  end
end
