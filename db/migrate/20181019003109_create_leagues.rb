class CreateLeagues < ActiveRecord::Migration[5.2]
  def change
    create_table :leagues do |t|
      t.string :tournament_code, null: false
      t.string :name, null: false

      t.timestamps
    end
  end
end
