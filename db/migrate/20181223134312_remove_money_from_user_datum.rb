class RemoveMoneyFromUserDatum < ActiveRecord::Migration[5.2]
  def change
    remove_column :user_data, :money, :decimal
  end
end
