class AddMandatoryMatchFieldsAndVisitorMatchFieldsToMatch < ActiveRecord::Migration[5.2]
  def change
    add_column :matches, :mandatory_goals_first_period, :integer, limit: 2, default: 0
    add_column :matches, :visitor_goals_first_period, :integer, limit: 2, default: 0
    add_column :matches, :mandatory_goals_last_period, :integer, limit: 2, default: 0
    add_column :matches, :visitor_goals_last_period, :integer, limit: 2, default: 0
    add_column :matches, :mandatory_yellow_cards, :integer, limit: 2, default: 0
    add_column :matches, :visitor_yellow_cards, :integer, limit: 2, default: 0
    add_column :matches, :mandatory_fouls, :integer, limit: 2, default: 0
    add_column :matches, :visitor_fouls, :integer, limit: 2, default: 0
    add_column :matches, :ball_possession, :integer, limit: 1, default: 0
  end
end
