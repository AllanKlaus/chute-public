class CreateUserData < ActiveRecord::Migration[5.2]
  def change
    create_table :user_data do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.date :birth
      t.decimal :money, precision: 10, scale: 2

      t.timestamps
    end
  end
end
