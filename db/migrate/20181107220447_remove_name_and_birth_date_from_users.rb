class RemoveNameAndBirthDateFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :name, :string
    remove_column :users, :birth_date, :date
  end
end
