class RemoveYearFromSeason < ActiveRecord::Migration[5.2]
  def change
    remove_column :seasons, :year, :integer
  end
end
