class AddFinishedToMatch < ActiveRecord::Migration[5.2]
  def change
    add_column :matches, :finished, :boolean, default: false
  end
end
