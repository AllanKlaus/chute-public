class CreateUserPoints < ActiveRecord::Migration[5.2]
  def change
    create_table :user_points do |t|
      t.references :user, foreign_key: true
      t.references :hint, foreign_key: true
      t.integer :points, limit: 3

      t.timestamps
    end
  end
end
