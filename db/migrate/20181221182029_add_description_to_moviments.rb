class AddDescriptionToMoviments < ActiveRecord::Migration[5.2]
  def change
    add_column :moviments, :description, :string
  end
end
