class AddPointsToHints < ActiveRecord::Migration[5.2]
  def change
    add_column :hints, :points, :integer, default: 0
  end
end
