class CreateHints < ActiveRecord::Migration[5.2]
  def change
    create_table :hints do |t|
      t.references :user, foreign_key: true, index: true
      t.references :season, foreign_key: true, index: true
      t.references :match, foreign_key: true, index: true, null: true
      t.integer :mandatory_goals_first_period, limit: 2
      t.integer :visitor_goals_first_period, limit: 2
      t.integer :mandatory_goals_last_period, limit: 2
      t.integer :visitor_goals_last_period, limit: 2
      t.integer :mandatory_yellow_cards, limit: 2
      t.integer :visitor_yellow_cards, limit: 2
      t.integer :mandatory_fouls, limit: 2
      t.integer :visitor_fouls, limit: 2
      t.integer :ball_possession, limit: 1

      t.timestamps
    end
  end
end
