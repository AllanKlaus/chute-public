# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_23_134401) do

  create_table "contacts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "subject"
    t.string "email"
    t.boolean "read", default: true
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "friendly_id_slugs", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, length: { slug: 70, scope: 70 }
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", length: { slug: 140 }
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "hints", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "match_id"
    t.integer "mandatory_goals_first_period", limit: 2
    t.integer "visitor_goals_first_period", limit: 2
    t.integer "mandatory_goals_last_period", limit: 2
    t.integer "visitor_goals_last_period", limit: 2
    t.integer "mandatory_yellow_cards", limit: 2
    t.integer "visitor_yellow_cards", limit: 2
    t.integer "mandatory_fouls", limit: 2
    t.integer "visitor_fouls", limit: 2
    t.integer "ball_possession", limit: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "points", default: 0
    t.index ["match_id"], name: "index_hints_on_match_id"
    t.index ["user_id"], name: "index_hints_on_user_id"
  end

  create_table "leagues", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "tournament_code", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "continent", limit: 2
  end

  create_table "matches", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "mandatory_id"
    t.bigint "visitor_id"
    t.date "match_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "season_id"
    t.string "match_code", null: false
    t.integer "mandatory_goals_first_period", limit: 2, default: 0
    t.integer "visitor_goals_first_period", limit: 2, default: 0
    t.integer "mandatory_goals_last_period", limit: 2, default: 0
    t.integer "visitor_goals_last_period", limit: 2, default: 0
    t.integer "mandatory_yellow_cards", limit: 2, default: 0
    t.integer "visitor_yellow_cards", limit: 2, default: 0
    t.integer "mandatory_fouls", limit: 2, default: 0
    t.integer "visitor_fouls", limit: 2, default: 0
    t.integer "ball_possession", limit: 1, default: 0
    t.boolean "finished", default: false
    t.index ["mandatory_id"], name: "index_matches_on_mandatory_id"
    t.index ["season_id"], name: "index_matches_on_season_id"
    t.index ["visitor_id"], name: "index_matches_on_visitor_id"
  end

  create_table "messages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.text "message"
    t.bigint "contact_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin", default: false
    t.index ["contact_id"], name: "index_messages_on_contact_id"
  end

  create_table "moviments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "type", limit: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.integer "money", default: 0
    t.index ["user_id"], name: "index_moviments_on_user_id"
  end

  create_table "points", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.integer "value"
    t.boolean "round_factor", default: false
  end

  create_table "rewards", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "position"
    t.decimal "award", precision: 10, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seasons", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "league_id"
    t.string "season_code", null: false
    t.date "start_date"
    t.date "end_date"
    t.integer "matches", limit: 3
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.index ["league_id"], name: "index_seasons_on_league_id"
  end

  create_table "teams", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.string "abbreviation", limit: 3, null: false
    t.string "shield"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "team_code", null: false
  end

  create_table "user_data", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.date "birth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "money", default: 0
    t.index ["user_id"], name: "index_user_data_on_user_id"
  end

  create_table "user_points", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "hint_id"
    t.integer "points", limit: 3
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hint_id"], name: "index_user_points_on_hint_id"
    t.index ["user_id"], name: "index_user_points_on_user_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "slug"
    t.string "name", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "contacts", "users"
  add_foreign_key "hints", "matches"
  add_foreign_key "hints", "users"
  add_foreign_key "matches", "seasons"
  add_foreign_key "messages", "contacts"
  add_foreign_key "moviments", "users"
  add_foreign_key "seasons", "leagues"
  add_foreign_key "user_data", "users"
  add_foreign_key "user_points", "hints"
  add_foreign_key "user_points", "users"
end
