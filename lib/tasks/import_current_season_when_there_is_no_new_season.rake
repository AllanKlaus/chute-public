# frozen_string_literal: true

module Rake
  class SeasonImporter
    attr_reader :season

    def self.execute(season_id:)
      new(season_id).execute
    end

    def initialize(season_id)
      @season = Season.find(season_id)
    end

    def execute
      import_matches
    end

    private

    def import_matches
      Api::MatchImporter.execute(season: season,
                                 matches: api_response[:sport_events])

      Match.where("match_date < '#{Date.current}'").update_all(finished: true)
    end

    def api_response
      @api_response ||= JSON.parse(Faraday.get(uri).body).with_indifferent_access
    end

    def uri
      "#{CONFIG_APP.external_api_url}" \
        "#{continent}" \
        "#{route}" \
        "#{CONFIG_APP.continent["#{continent}_api_key"]}"
    end

    def route
      CONFIG_APP.season_route.gsub(CONFIG_APP.season_route_overwrite, season.tournament_code)
    end

    def continent
      season.continent
    end
  end
end

namespace :import do
  desc 'Import matches'
  task matches: [:environment] do
    Season.all.each do |season|
      Rake::SeasonImporter.execute(season_id: season.id)
      sleep 61
    end
  end
end
