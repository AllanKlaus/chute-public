# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

gem 'mysql2'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.1'
gem 'sass-rails', '~> 5.0'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'
gem 'xmlrpc'

# Security
gem 'bcrypt'

# Lifecycle management
gem 'foreman'

# Monitoring
gem 'sentry-raven'

# Background job
gem 'redis', '~> 4.0'
gem 'sidekiq'
gem 'sidekiq-failures'
gem 'sidekiq-scheduler'

# Login
gem 'devise'
gem 'devise-i18n'
gem 'gravatar-ultimate'

# System
gem 'friendly_id'
gem 'jquery-rails'

# Pagination
gem 'bootstrap-kaminari-views'
gem 'kaminari'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'bootsnap', '>= 1.1.0', require: false

# Seed
gem 'ffaker'

group :development, :test do
  gem 'brakeman', require: false
  gem 'bundler-audit', require: false
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_bot_rails'
  gem 'mutant-rspec', require: false
  gem 'rails_best_practices', require: false
  gem 'rspec-rails', require: false
  gem 'rubocop', require: false
  gem 'rubycritic'
  gem 'rufo', require: false
  gem 'simplecov', require: false
  gem 'solargraph', require: false
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'chromedriver-helper'
  gem 'rspec-sidekiq'
  gem 'selenium-webdriver'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
