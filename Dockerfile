FROM allanklaus/ruby:2.5.3

RUN echo 'gem: --no-document' > /root/.gemrc

RUN apt-get update -qq && \
    apt-get install -y --no-install-recommends \
    libxslt-dev && rm -rf /var/lib/apt/lists/*

ENV APP_HOME /chute-feito
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

ADD . $APP_HOME

RUN chmod -R 777 $APP_HOME/tmp/
RUN chmod -R 777 $APP_HOME/data/
RUN chmod -R 777 $APP_HOME/public/assets/
RUN chmod -R 755 $APP_HOME/vendor/

ENV RAILS_ENV=production

ADD Gemfile* $APP_HOME/
RUN gem install bundle
RUN bundle config build.nokogiri --use-system-libraries
RUN bundle install

VOLUME ["$APP_HOME/public"]

CMD /bin/bash -l -c "bundle exec rake assets:precompile && bundle exec foreman start"
