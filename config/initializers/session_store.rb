# Be sure to restart your server when you modify this file.

Rails.application.config.session_store :cache_store, key: ENV['APP_SESSION_KEY']
