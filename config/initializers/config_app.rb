class ConfigApp
  def initialize
    config_yml.each do |key, value|
      self.instance_variable_set("@#{key}", value)
      self.class.send(:define_method, key, proc{self.instance_variable_get("@#{key}")})
      self.class.send(:define_method, "#{key}=", proc{ |value| self.instance_variable_set("@#{key}", value)})
    end
  end

  private

  def config_yml
    YAML.load_file(Rails.root.join('config/config.yml'))[Rails.env]
  end
end

CONFIG_APP = ConfigApp.new