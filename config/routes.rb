Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions',
  }

  mount Sidekiq::Web => '/sidekiq'

  root 'home#index'
  get 'dashboard', to: 'home#dashboard'
  get 'terms', to: 'home#terms'

  resources :matches, only: [:index, :show] do
    collection do
      get 'ended'
    end

    member do
      resources :hints, as: :match_hint, only: :new
    end
  end

  resources :hints, only: [:index, :show, :create]

  resources :users, only: [:show] do
    collection do
      get 'dashboard'
      get 'ranking'
    end
  end

  resources :contacts, only: [:index, :show, :new, :create] do
    collection do
      post 'answer_message'
    end

    member do
      get 'answer'
    end
  end

  resources :moviments, only: [:index, :create]
end
