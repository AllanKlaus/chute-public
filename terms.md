1. Motivação
  Hoje em dia nos todos nos acabamos brincando e apostando com nossos amigos sobre os jogos, a ideia do Chute Feito é você
  continuar dando seus chutes nos resultados de jogos, mas ganhar prêmios com isso, tornando a brincadeira mais divertida.

2. Quem pode participar?
  Todos podem participar, não há nenhum tipo de restrição.

3. Prêmio
  O prêmio mensal são 10 "chuteiras virtuais", cada chuteira equivale a R$ 1,00 que podem ser reventidos em cartões presentes em lojas virtuais. Quando o jogador tiver um mínimo de 50 chuteiras virtuais pode reverter as chuteiras em cartão presente em uma das lojas listadas no item 8.

4. Como ganhar?
  Para ganhar o prêmio você tem que fazer mais pontos com os seus chutes do que os seus concorrentes. O jogador que fizer mais pontos no mês ganha o prêmio.

5. Chutes
  Em cada partida você poderá dar chutes nos seguintes itens:
    - Número de gols por tempo e por time
    - Número de cartões amarelos por time
    - Número de faltas por time

6. Pontuação
  Cada chute tem uma pontuação que é a seguinte:
  - Acertar o placar exato da partida<sup>1</sup>: 70 pontos
  - Acertar o vendedor da partida<sup>2</sup>: 30 pontos
  - Acertar o número de faltas de um dos times<sup>3</sup>: 10 pontos
  - Acertar o número de cartões amarelos de um dos times<sup>3</sup>: 10 pontos
  - Acertar o número total de gols da partida<sup>4</sup>: 5 pontos
  - Acertar o número total de faltas da partida<sup>4</sup>: 3 pontos
  - Acertar o número total de cartões amarelos da partida<sup>4</sup>: 3 pontos
  - Acertar o número de gols por tempo de um dos times<sup>5</sup>: 2 pontos

7. Regras
  Somente pode ser dado um chute por partida.
  Os chutes precisam ser dados um dia antes da partida, se a partida ocorrer no dia 11/11, o jogador só pode dar o chute até o dia 10/11
  - Em caso de empates na pontuação, os critérios de desempate são:
    1. O jogador que fez mais pontos em uma única partida
    2. O jogador que é cadastrado a mais tempo no site

8. As lojas participantes
  Cada loja tem as suas próprias regras de como o cartão presente deve ser utilizado, por isso antes de selecionar a loja que você deseja é importante ler as regras de cada uma delas. As lojas participantes são:

    - Americanas - Valor mínimo<sup>6</sup> R$50,00, Valor máximo<sup>7</sup> R$ 200,00
    - Centauro - Valor mínimo<sup>6</sup> R$50,00, Valor máximo<sup>7</sup> R$ 1000,00
    - Livraria Cultura - Valor mínimo<sup>6</sup> R$50,00, Valor máximo<sup>7</sup> R$ 1000,00
    - Saraiva - Valor mínimo<sup>6</sup> R$50,00, Valor máximo<sup>7</sup> R$ 1500,00

  <sup>1</sup> O placar exato consistem em acertar o número de gols que cada time fez exatamente com o resultado real do jogo. Se o placar foi 2x1 para o mandante, então para ganhar os pontos o chute tem que ser 2x1 para o mandante.
  <sup>2</sup> O vendedor da partida é aquele que faz mais gols, para ganhar os pontos basta acertar quem fez mais gols na partida.
  <sup>3</sup> Se no palpite o jogador acertar em questão de ambos os times irá ganhar os pontos por cada acerto.
  <sup>4</sup> Esse número e referente ao valores absolutos da partida, somando o valor em questão de ambos os times. Ex.: Número de gols da partida é a soma do número de gols dos dois times, se o placar da partida foi 2x1 e o jogador deu um palpite de 3x0 ele irá ganhar os pontos.
  <sup>5</sup> Se o mandante fizer 1 gol no primeiro tempo e 2 no segundo e jogador no seu chute colocar exatamente igual o jogador fará 4 pontos. Sendo assim a cada acerto de gols de um time no tempo certo o jogador faz os pontos
  <sup>6</sup> O valor mínimo que a loja oferece no valor do cupom ou aquilo que o site Chute Feito determina como o mínimo para adquirir o cartão presente.
  <sup>7</sup> O valor máximo que a loja oferece no valor do cupom, sendo que se o jogador tiver um valor disponível para troca maior do que o valor máximo permitido pela loja, a troca se limitará ao valor máximo que a loja oferece.