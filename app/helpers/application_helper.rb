# frozen_string_literal: true

module ApplicationHelper
  def match_text_status(match)
    @match_text_status ||= match.finished? ? I18n.t('see_result') : I18n.t('do_hint')
  end

  def message_user(admin)
    return I18n.t('generic.admin') if admin

    I18n.t('generic.you')
  end

  def render_rescue_form(money)
    partial('moviments/partials/rescue_form', money >= MovimentsServices::Creator::MINIMUM_RESCUE)
  end

  def flash_message
    messages = ''

    %i[notice info warning error].each do |type|
      messages += "<p class=\"#{type}\">#{flash[type]}</p>" if flash[type]
    end

    messages
  end

  def moviment_icon(type)
    return I18n.t('moviments.index.moviment_type.in_html').html_safe if type == 'in'

    I18n.t('moviments.index.moviment_type.out_html').html_safe
  end

  def partial(partial, condition)
    render partial: partial if condition
  end
end
