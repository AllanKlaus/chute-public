var hints = hints || {};

hints.new = (function () {
  "use strict";

  function calculateGoals() {
    $('.mandatory-goals').on('change', updateMandatoryGoals);
    $('.visitor-goals').on('change', updateVisitorGoals);
  }

  function updateMandatoryGoals() {
    var sum = 0;
    $('.mandatory-goals').each(function () {
      sum += parseInt($(this).val());
    });

    $('#mandatory-result').html(sum);
  }

  function updateVisitorGoals() {
    var sum = 0;
    $('.visitor-goals').each(function () {
      sum += parseInt($(this).val());
    });

    $('#visitor-result').html(sum);
  }

  function init() {
    calculateGoals();
  }

  return {
    init: init
  };
}());