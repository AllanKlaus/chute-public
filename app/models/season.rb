# frozen_string_literal: true

class Season < ApplicationRecord
  belongs_to :league

  scope :to_check, -> { where('active = true AND end_date < ?', Date.current) }

  delegate :tournament_code, :continent, to: :league
  delegate :name, to: :league, prefix: true
end
