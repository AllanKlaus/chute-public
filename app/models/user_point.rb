# frozen_string_literal: true

class UserPoint < ApplicationRecord
  belongs_to :user
  belongs_to :hint

  delegate :match_date, to: :hint
end
