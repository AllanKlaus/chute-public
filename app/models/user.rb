# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  extend FriendlyId

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :hints
  has_many :matches, through: :hints
  has_one :user_datum

  friendly_id :name, use: :slugged

  def hints_count
    hints.count
  end
end
