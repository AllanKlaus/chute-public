# frozen_string_literal: true

class Match < ApplicationRecord
  belongs_to :mandatory, class_name: 'Team'
  belongs_to :visitor, class_name: 'Team'
  belongs_to :season

  enum ball_possession: %i[draw mandatory visitor]

  WEEK_DATES = (Date.current.at_beginning_of_week..Date.current.at_end_of_week)

  scope :to_check, -> { where('finished = false AND match_date < ?', Date.current) }
  scope :ended, -> { where(finished: true) }
  scope :this_week, -> { where(match_date: WEEK_DATES).order(:match_date) }
  scope :not_ended, -> { where(finished: false) }

  delegate :continent, :league_name, to: :season
  delegate :name, to: :mandatory, prefix: true
  delegate :name, to: :visitor, prefix: true

  def mandatory_goals
    mandatory_goals_first_period + mandatory_goals_last_period
  end

  def visitor_goals
    visitor_goals_first_period + visitor_goals_last_period
  end

  def finished?
    finished || match_date.eql?(Date.current)
  end

  def winner
    HintsServices::Winner.retrieve(self)
  end
end
