# frozen_string_literal: true

class Moviment < ApplicationRecord
  belongs_to :user

  enum type: %i[in out]

  self.inheritance_column = '_type'
end
