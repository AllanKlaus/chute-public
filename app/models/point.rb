# frozen_string_literal: true

class Point < ApplicationRecord
  def self.scoreboard
    Point.find_by(name: 'scoreboard').value
  end

  def self.winner
    Point.find_by(name: 'winner').value
  end

  def self.team_fouls
    Point.find_by(name: 'team_fouls').value
  end

  def self.team_yellow_cards
    Point.find_by(name: 'team_yellow_cards').value
  end

  def self.all_goals
    Point.find_by(name: 'all_goals').value
  end

  def self.all_fouls
    Point.find_by(name: 'all_fouls').value
  end

  def self.all_yellow_cards
    Point.find_by(name: 'all_yellow_cards').value
  end

  def self.team_goals_per_period
    Point.find_by(name: 'team_goals_per_period').value
  end

  def self.ball_possession
    Point.find_by(name: 'ball_possession').value
  end
end
