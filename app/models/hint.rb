# frozen_string_literal: true

class Hint < ApplicationRecord
  belongs_to :user
  belongs_to :match

  scope :by_user, ->(user) { where(user: user).order(:created_at) }

  TYPES = {
    yellow_cards: {
      hint: {
        mandatory: ->(hint) { hint.mandatory_yellow_cards },
        visitor: ->(hint) { hint.visitor_yellow_cards }
      },
      match: {
        mandatory: ->(hint) { hint.match_mandatory_yellow_cards },
        visitor: ->(hint) { hint.match_visitor_yellow_cards }
      }
    },
    fouls: {
      hint: {
        mandatory: ->(hint) { hint.mandatory_fouls },
        visitor: ->(hint) { hint.visitor_fouls }
      },
      match: {
        mandatory: ->(hint) { hint.match_mandatory_fouls },
        visitor: ->(hint) { hint.match_visitor_fouls }
      }
    }
  }.freeze

  delegate :mandatory_name,
           :visitor_name,
           :match_date,
           :league_name, to: :match

  delegate :mandatory_goals_first_period,
           :mandatory_goals_last_period,
           :mandatory_yellow_cards,
           :mandatory_fouls,
           :visitor_goals_first_period,
           :visitor_goals_last_period,
           :visitor_yellow_cards,
           :visitor_fouls,
           :mandatory_goals,
           :visitor_goals,
           :winner,
           :finished?, to: :match, prefix: true

  def mandatory_goals
    mandatory_goals_first_period + mandatory_goals_last_period
  end

  def visitor_goals
    visitor_goals_first_period + visitor_goals_last_period
  end

  def winner
    HintsServices::Winner.retrieve(self)
  end

  def hint_type_team(type, team)
    TYPES[type][:hint][team].call(self)
  end

  def match_type_team(type, team)
    TYPES[type][:match][team].call(self)
  end
end
