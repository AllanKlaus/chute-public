# frozen_string_literal: true

class UserDatum < ApplicationRecord
  belongs_to :user
end
