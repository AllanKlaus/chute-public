# frozen_string_literal: true

class GetMatchResultJob < ApplicationJob
  queue_as :season_jobs

  def perform(match_id)
    Api::MatchValidator.execute(match_id: match_id)
  end
end
