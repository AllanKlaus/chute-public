# frozen_string_literal: true

class CheckMatchFinishedJob < ApplicationJob
  queue_as :season_jobs

  def perform
    Api::MatchChecker.execute
  end
end
