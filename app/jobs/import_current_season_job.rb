# frozen_string_literal: true

class ImportCurrentSeasonJob < ApplicationJob
  queue_as :season_jobs

  def perform(season_id)
    Api::SeasonImporter.execute(season_id: season_id)
  end
end
