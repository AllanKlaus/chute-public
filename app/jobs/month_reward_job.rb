# frozen_string_literal: true

class MonthRewardJob < ApplicationJob
  queue_as :reward_jobs

  def perform
    PointsServices::MonthReward.execute
  end
end
