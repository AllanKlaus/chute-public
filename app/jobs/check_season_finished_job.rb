# frozen_string_literal: true

class CheckSeasonFinishedJob < ApplicationJob
  queue_as :season_jobs

  def perform
    Api::SeasonChecker.execute
  end
end
