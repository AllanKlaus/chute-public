# frozen_string_literal: true

class CalculateUserPointsJob < ApplicationJob
  queue_as :hint_jobs

  def perform(match_id)
    HintsServices::PointsCalculator.execute(match_id: match_id)
  end
end
