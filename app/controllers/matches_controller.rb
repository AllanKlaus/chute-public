# frozen_string_literal: true

class MatchesController < ApplicationController
  def index
    @matches = Match.not_ended.this_week.page(params[:page] || 0)
  end

  def show
    @match = Match.find(params_id)
  end

  def ended
    @matches = Match.ended.order(id: :desc).page(params[:page] || 0)
    render :index
  end
end
