# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :authenticate_user!, only: :dashboard

  def show
    user = User.friendly.find(params[:id])

    @page = {
      user: user,
      points: PointsServices::Dashboard.execute(user),
      gravatar: Gravatar.new(user.email).image_url,
      hints_count: user.hints_count
    }
  end

  def dashboard
    @page = {
      user: current_user,
      points: PointsServices::Dashboard.execute(current_user),
      next_matches: Match.not_ended.this_week,
      hints_count: current_user.hints_count
    }
  end

  def ranking
    @ranking = PointsServices::Ranking
               .execute((Date.current.at_beginning_of_month..Date.current.at_end_of_month))
               .page(params[:page] || 0)
  end
end
