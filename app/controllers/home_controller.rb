# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    matches_ended = Match.ended
    @page = {
      counters: index_hash_counters(matches_ended),
      last_finished_match: matches_ended.last,
      next_matches: Match.not_ended.this_week.first(5)
    }
  end

  private

  def index_hash_counters(matches_ended)
    {
      players: User.count,
      finished_matches: matches_ended.count,
      hints: Hint.count,
      points: UserPoint.all.map(&:points).sum
    }
  end
end
