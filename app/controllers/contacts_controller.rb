# frozen_string_literal: true

class ContactsController < ApplicationController
  before_action :authenticate_user!, except: %i[new create]

  def index
    @contacts = Contact.where(user: current_user).page(params[:page] || 0)
  end

  def show
    load_contact
  end

  def create
    contact = Contact.create(subject: params[:subject], email: params[:email], user: user_logged)

    Message.create(contact: contact, message: params[:message])

    return redirect_to contacts_path if current_user

    redirect_to new_contact_path
  end

  def answer
    load_contact
  end

  def answer_message
    contact = Contact.find_by!(user: current_user, id: params[:id])
    Message.create(contact: contact, message: params[:message])
    redirect_to contact_path(contact)
  rescue StandardError
    redirect_to new_contact_path
  end

  private

  def user_logged
    current_user || User.first
  end

  def load_contact
    @contact = Contact.find_by!(user: current_user, id: params[:id])
  rescue StandardError
    redirect_to new_contact_path
  end
end
