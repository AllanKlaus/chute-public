# frozen_string_literal: true

class HintsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_match, only: :create

  def index
    @hints = Hint.by_user(current_user).page(params[:page] || 0)
  end

  def show
    @hint = Hint.find_by(id: params_id, user: current_user)
  end

  def new
    @match = Match.find(params_id)
  end

  def create
    HintsServices::Creator.execute(params: hint_params, match: @match, user: current_user)
    redirect_to hints_path
  end

  private

  def hint_params
    params.require(:hint).permit(:mandatory_goals_first_period,
                                 :visitor_goals_first_period,
                                 :mandatory_goals_last_period,
                                 :visitor_goals_last_period,
                                 :mandatory_yellow_cards,
                                 :visitor_yellow_cards,
                                 :mandatory_fouls,
                                 :visitor_fouls)
  end

  def set_match
    @match = Match.find(params[:match_id])
  end
end
