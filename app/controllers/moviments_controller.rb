# frozen_string_literal: true

class MovimentsController < ApplicationController
  before_action :authenticate_user!

  def index
    @page = {
      moviments: Moviment.where(user: current_user).order(id: :desc),
      user_data: UserDatum.find_or_create_by(user: current_user)
    }
  end

  def create
    type, message = MovimentsServices::Creator.execute(user: current_user, params: params)
    flash[type] = message

    redirect_to moviments_path
  end
end
