# frozen_string_literal: true

module MovimentsServices
  class Creator
    private_class_method :new

    attr_reader :params, :user

    MINIMUM_RESCUE = 50

    def self.execute(params:, user:)
      new(params, user).execute
    end

    def initialize(params, user)
      @params = params
      @user = user
    end

    def execute
      return [:error, I18n.t('moviments.index.messages.error')] unless can_moviment?

      create_moviment
      [:info, I18n.t('moviments.index.messages.success')]
    end

    private

    def can_moviment?
      user_data.money >= money && money >= MINIMUM_RESCUE
    end

    def user_data
      @user_data ||= UserDatum.find_by(user: user)
    end

    def money
      @money ||= params[:rescue].to_i
    end

    def create_moviment
      Moviment.create(user: user, money: money, type: :out, description: params[:store])
      user_data.money -= money
      user_data.save
    end
  end
end
