# frozen_string_literal: true

module MatchServices
  class ResultParser
    private_class_method :new

    PERIODS = { first: 1, second: 2 }.freeze
    QUALIFIER = { mandatory: 'home', visitor: 'away' }.freeze
    BALL_POSSESSION = { draw: 0, mandatory: 1, visitor: 2 }.freeze
    BALL_POSSESSION_DIVISOR = 50

    attr_reader :api_response

    def self.execute(api_response)
      new(api_response).execute
    end

    def initialize(api_response)
      @api_response = api_response
    end

    def execute
      { finished: true }.merge(yellow_cards)
                        .merge(fouls)
                        .merge(match_period_score)
                        .merge(ball_possession)
    end

    private

    def match_period_scores
      @match_period_scores ||= api_response[:sport_event_status][:period_scores]
    end

    def first_period_scores
      @first_period_scores ||= period_scores(PERIODS[:first])
    end

    def last_period_scores
      @last_period_scores ||= period_scores(PERIODS[:second])
    end

    def mandatory_statistics
      @mandatory_statistics ||= team_statistics(QUALIFIER[:mandatory])
    end

    def visitor_statistics
      @visitor_statistics ||= team_statistics(QUALIFIER[:visitor])
    end

    def first_team
      @first_team ||= api_response[:statistics][:teams].first
    end

    def last_team
      @last_team ||= api_response[:statistics][:teams].last
    end

    def team_statistics(qualifier)
      return first_team[:statistics] if first_team[:qualifier].eql?(qualifier)

      last_team[:statistics]
    end

    def period_scores(period)
      return match_period_scores.first if match_period_scores.first[:number].eql?(period)

      match_period_scores.last
    end

    def greater_ball_possession
      if mandatory_statistics[:ball_possession] > BALL_POSSESSION_DIVISOR
        BALL_POSSESSION[:mandatory]
      elsif visitor_statistics[:ball_possession] > BALL_POSSESSION_DIVISOR
        BALL_POSSESSION[:visitor]
      else
        BALL_POSSESSION[:draw]
      end
    end

    def yellow_cards
      {
        mandatory_yellow_cards: mandatory_statistics[:yellow_cards],
        visitor_yellow_cards: visitor_statistics[:yellow_cards]
      }
    end

    def fouls
      { mandatory_fouls: mandatory_statistics[:fouls], visitor_fouls: visitor_statistics[:fouls] }
    end

    def match_period_score
      {
        mandatory_goals_first_period: first_period_scores[:home_score],
        visitor_goals_first_period: first_period_scores[:away_score],
        mandatory_goals_last_period: last_period_scores[:home_score],
        visitor_goals_last_period: last_period_scores[:away_score]
      }
    end

    def ball_possession
      { ball_possession: greater_ball_possession }
    end
  end
end
