# frozen_string_literal: true

module Api
  class SeasonChecker
    def self.execute
      Season.to_check.each do |season|
        ImportCurrentSeasonJob.perform_now(season.id)
      end
    end
  end
end
