# frozen_string_literal: true

module Api
  class MatchImporter
    TEAM_KIND = {
      mandatory: 'home',
      visitor: 'away'
    }.freeze

    attr_accessor :season, :matches

    def self.execute(season:, matches:)
      new(season, matches).execute
    end

    def initialize(season, matches)
      @season = season
      @matches = matches
    end

    def execute
      matches.each do |match|
        Match.create(
          match_date: match[:scheduled],
          match_code: match[:id],
          season: season,
          mandatory: select_team(TEAM_KIND[:mandatory], match[:competitors]),
          visitor: select_team(TEAM_KIND[:visitor], match[:competitors])
        )
      end
    end

    private

    def select_team(qualifier, competitors)
      competitor = competitors.select do |team|
        self if team[:qualifier].eql?(qualifier)
      end.first

      team(competitor)
    end

    def team(competitor)
      Team.find_by(team_code: competitor[:id]) || TeamCreator.execute(competitor)
    end
  end
end
