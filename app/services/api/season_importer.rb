# frozen_string_literal: true

module Api
  class SeasonImporter
    attr_reader :season

    def self.execute(season_id:)
      new(season_id).execute
    end

    def initialize(season_id)
      @season = Season.find(season_id)
    end

    def execute
      import_season if new_season?
    end

    private

    def new_season?
      !Date.parse(api_season[:start_date]).eql?(season.start_date) &&
        !Date.parse(api_season[:end_date]).eql?(season.end_date)
    end

    def import_season
      create_season
      deactivate_current_season
      import_matches
    end

    def create_season
      Season.create(
        season_code: api_season[:id],
        start_date: api_season[:start_date],
        end_date: api_season[:end_date],
        matches: api_response[:sport_events].count,
        league: season.league
      )
    end

    def deactivate_current_season
      season.active = false
      season.save!
    end

    def import_matches
      Api::MatchImporter.execute(season: season,
                                 matches: api_response[:sport_events])
    end

    def api_response
      @api_response ||= JSON.parse(Faraday.get(uri).body).with_indifferent_access
    end

    def api_season
      @api_season ||= api_response[:sport_events].first[:season]
    end

    def uri
      Router.execute(season.continent, route)
    end

    def route
      CONFIG_APP.season_route.gsub(CONFIG_APP.season_route_overwrite, season.tournament_code)
    end
  end
end
