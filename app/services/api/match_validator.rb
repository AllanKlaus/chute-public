# frozen_string_literal: true

module Api
  class MatchValidator
    MATCH_FINISHED = 'ended'

    attr_reader :match

    def self.execute(match_id:)
      new(match_id).execute
    end

    def initialize(match_id)
      @match = Match.find(match_id)
    end

    def execute
      return unless match_finished?

      update_match
      calculate_points
    end

    private

    def update_match
      match.update_attributes(match_result)
    end

    def match_result
      @match_result ||= MatchServices::ResultParser.execute(api_response)
    end

    def calculate_points
      CalculateUserPointsJob.perform_later(match.id)
    end

    def api_response
      @api_response ||= JSON.parse(Faraday.get(uri).body).with_indifferent_access
    end

    def uri
      Router.execute(match.continent, route)
    end

    def route
      CONFIG_APP.match_route.gsub(CONFIG_APP.match_route_overwrite, match.match_code)
    end

    def match_finished?
      api_response[:sport_event_status][:match_status].eql?(MATCH_FINISHED)
    end
  end
end
