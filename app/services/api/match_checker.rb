# frozen_string_literal: true

module Api
  class MatchChecker
    def self.execute
      Match.to_check.each_with_index do |match, index|
        next_job = index + 1
        GetMatchResultJob.set(wait: next_job.minutes).perform_later(match.id)
      end
    end
  end
end
