# frozen_string_literal: true

module Api
  class Router
    def self.execute(continent, route)
      "#{CONFIG_APP.external_api_url}" \
        "#{continent}" \
        "#{route}" \
        "#{CONFIG_APP.continent["#{continent}_api_key"]}"
    end
  end
end
