# frozen_string_literal: true

module Api
  class TeamCreator
    attr_accessor :competitor

    def self.execute(competitor)
      new(competitor).execute
    end

    def initialize(competitor)
      @competitor = competitor
    end

    def execute
      Team.create(
        name: competitor[:name],
        abbreviation: competitor[:abbreviation],
        team_code: competitor[:id]
      )
    end
  end
end
