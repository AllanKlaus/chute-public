# frozen_string_literal: true

module HintsServices
  class Creator
    private_class_method :new

    attr_reader :params, :match, :user

    def self.execute(params:, match:, user:)
      new(params, match, user).execute
    end

    def initialize(params, match, user)
      @params = params
      @match = match
      @user = user
    end

    def execute
      return if invalid?

      Hint.create(hint)
    end

    private

    def invalid?
      match.finished? || hint_exists?
    end

    def hint_exists?
      user.matches.map(&:id).include?(match.id)
    end

    def hint
      params.merge(
        match: match,
        user: user
      )
    end
  end
end
