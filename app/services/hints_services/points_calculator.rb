# frozen_string_literal: true

module HintsServices
  class PointsCalculator
    private_class_method :new

    attr_reader :hints

    TYPES = {
      fouls: :fouls,
      yellow_cards: :yellow_cards
    }.freeze

    def self.execute(match_id:)
      new(match_id).execute
    end

    def initialize(match_id)
      @match = Match.find(match_id)
      @hints = Hint.where(match: @match)
    end

    def execute
      raise I18n.t('errors.match_not_finished') unless @match.finished?

      calculate_points
    end

    private

    def calculate_points
      hints.find_each do |hint|
        points = calculate(hint)
        create_points(hint, points)
      end
    end

    def calculate(hint)
      points = 0
      points += GoalsCalculator.execute(hint: hint)
      points += BasicCalculator.execute(params(hint, TYPES[:fouls], fouls_points))
      points += BasicCalculator.execute(params(hint, TYPES[:yellow_cards], yellow_cards_points))
      points += assert_hint_winner(hint)

      points
    end

    def assert_hint_winner(hint)
      hint.winner == hint.match_winner ? Point.winner : 0
    end

    def create_points(hint, points)
      UserPoint.create(hint: hint, user: hint.user, points: points)
    end

    def params(hint, type, points)
      calculator_params(hint, type).merge(points)
    end

    def calculator_params(hint, type)
      {
        hint: hint,
        type: type
      }
    end

    def fouls_points
      {
        team_points: Point.team_fouls,
        all_points: Point.all_fouls
      }
    end

    def yellow_cards_points
      {
        team_points: Point.team_yellow_cards,
        all_points: Point.all_yellow_cards
      }
    end
  end
end
