# frozen_string_literal: true

module HintsServices
  class Winner
    private_class_method :new

    attr_reader :match

    WINNER = {
      draw: :draw,
      mandatory: :mandatory,
      visitor: :visitor
    }.freeze

    def self.retrieve(match)
      new(match).execute
    end

    def initialize(match)
      @match = match
    end

    def execute
      return WINNER[:mandatory] if match.mandatory_goals > match.visitor_goals
      return WINNER[:visitor] if match.mandatory_goals < match.visitor_goals

      WINNER[:draw]
    end
  end
end
