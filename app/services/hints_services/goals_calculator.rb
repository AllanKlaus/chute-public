# frozen_string_literal: true

module HintsServices
  class GoalsCalculator
    private_class_method :new

    attr_reader :hint, :points

    def self.execute(hint:)
      new(hint).execute
    end

    def initialize(hint)
      @hint = hint
      @points = 0
    end

    def execute
      assert_goals_per_period
      assert_all_goals
      assert_scoreboard

      @points
    end

    private

    def points_on_goals_assert
      @points_on_goals_assert ||= Point.team_goals_per_period
    end

    def points_on_all_goals_assert
      @points_on_all_goals_assert ||= Point.all_goals
    end

    def points_on_scoreboard_assert
      @points_on_scoreboard_assert ||= Point.scoreboard
    end

    def assert_all_goals
      @points += points_on_all_goals_assert if hint_goals == match_goals
    end

    def assert_scoreboard
      @points += points_on_scoreboard_assert if hint.mandatory_goals == hint.match_mandatory_goals &&
                                                hint.visitor_goals == hint.match_visitor_goals
    end

    def assert_goals_per_period
      @points += points_on_goals_assert if assert_mandatory_goals_on_first_period?
      @points += points_on_goals_assert if assert_mandatory_goals_on_last_period?
      @points += points_on_goals_assert if assert_visitor_goals_on_first_period?
      @points += points_on_goals_assert if assert_visitor_goals_on_last_period?
    end

    def hint_goals
      @hint_goals ||= hint.mandatory_goals + hint.visitor_goals
    end

    def match_goals
      @match_goals ||= hint.match_mandatory_goals + hint.match_visitor_goals
    end

    def assert_mandatory_goals_on_first_period?
      hint.mandatory_goals_first_period == hint.match_mandatory_goals_first_period
    end

    def assert_mandatory_goals_on_last_period?
      hint.mandatory_goals_last_period == hint.match_mandatory_goals_last_period
    end

    def assert_visitor_goals_on_first_period?
      hint.visitor_goals_first_period == hint.match_visitor_goals_first_period
    end

    def assert_visitor_goals_on_last_period?
      hint.visitor_goals_last_period == hint.match_visitor_goals_last_period
    end
  end
end
