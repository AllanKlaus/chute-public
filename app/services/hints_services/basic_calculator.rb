# frozen_string_literal: true

module HintsServices
  class BasicCalculator
    private_class_method :new

    attr_reader :hint, :team_points, :all_points, :type

    TYPES = {
      yellow_cards: {
        hint: {
          mandatory: 'mandatory_yellow_cards',
          vititor: 'vititor_yellow_cards'
        },
        match: {
          mandatory: 'match_mandatory_yellow_cards',
          vititor: 'match_vititor_yellow_cards'
        }
      },
      fouls: {
        hint: {
          mandatory: 'mandatory_fouls',
          vititor: 'vititor_fouls'
        },
        match: {
          mandatory: 'match_mandatory_fouls',
          vititor: 'match_vititor_fouls'
        }
      }
    }.freeze

    def self.execute(hint:, team_points:, all_points:, type:)
      new(hint, team_points, all_points, type).execute
    end

    def initialize(hint, team_points, all_points, type)
      @hint = hint
      @team_points = team_points
      @all_points = all_points
      @type = type
      @points = 0
    end

    def execute
      assert_all_type
      assert_team_type

      @points
    end

    private

    def assert_all_type
      @points += all_points if hint_type == match_type
    end

    def assert_team_type
      @points += team_points if assert_mandatory_type?
      @points += team_points if assert_visitor_type?
    end

    def hint_type
      @hint_type ||= hint.hint_type_team(type, :mandatory) + hint.hint_type_team(type, :visitor)
    end

    def match_type
      @match_type ||= hint.match_type_team(type, :mandatory) + hint.match_type_team(type, :visitor)
    end

    def assert_mandatory_type?
      hint.hint_type_team(type, :mandatory) == hint.match_type_team(type, :mandatory)
    end

    def assert_visitor_type?
      hint.hint_type_team(type, :visitor) == hint.match_type_team(type, :visitor)
    end
  end
end
