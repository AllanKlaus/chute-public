# frozen_string_literal: true

module PointsServices
  class Ranking
    private_class_method :new

    attr_reader :dates

    RANKING_GROUP_PARAMS = 'users.name, users.slug, users.id'
    RANKING_SQL_PARAMS = Arel.sql("SUM(user_points.points), #{RANKING_GROUP_PARAMS}")

    def self.execute(dates)
      new(dates).execute
    end

    def initialize(dates)
      @dates = dates
    end

    def execute
      new_ranking = UserPoint.select(RANKING_SQL_PARAMS)
                             .joins(:hint)
                             .joins(hint: :user)
                             .joins(hint: :match)
                             .where(hint: { matches: { match_date: dates } })
                             .group(Arel.sql(RANKING_GROUP_PARAMS))
                             .pluck(RANKING_SQL_PARAMS)

      new_ranking.sort! { |first, last| last[0] <=> first[0] }
      Kaminari.paginate_array(new_ranking)
    end
  end
end
