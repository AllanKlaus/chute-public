# frozen_string_literal: true

module PointsServices
  class MonthReward
    private_class_method :new

    def self.execute
      new.execute
    end

    def execute
      apply_reward
    end

    private

    def ranking
      @ranking ||= PointsServices::Ranking.execute(last_month)
    end

    def last_month
      @last_month ||= (any_date_last_month.at_beginning_of_month..any_date_last_month.at_end_of_month)
    end

    def any_date_last_month
      @any_date_last_month ||= Date.current - 1.month
    end

    def user_from_ranking_position(position)
      ranking.first(position).last
    end

    def apply_reward
      Reward.all.each do |reward|
        user = User.find(user_from_ranking_position(reward.position)[3])
        Moviment.create(user: user, money: reward.award, type: :in)
        update_user_data(user, reward.award)
      end
    end

    def update_user_data(user, award)
      user_data = UserDatum.find_or_create_by(user: user)
      user_data.money = 0 if user_data.money.blank?
      user_data.money += award
      user_data.save!
    end
  end
end
