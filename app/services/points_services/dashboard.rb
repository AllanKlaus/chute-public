# frozen_string_literal: true

module PointsServices
  class Dashboard
    private_class_method :new

    attr_reader :user

    def self.execute(user)
      new(user).execute
    end

    def initialize(user)
      @user = user
    end

    def execute
      {
        total_points: total_points,
        year_points: date_points(year_dates),
        last_month_points: date_points(last_month_dates),
        month_points: date_points(month_dates),
        week_points: date_points(week_dates)
      }
    end

    private

    def total_points
      UserPoint.where(user: user).map(&:points).sum
    end

    def date_points(dates)
      UserPoint.where(user: user).select { |point| dates.include?(point.match_date) }
               .map(&:points)
               .sum
    end

    def year_dates
      @year_dates ||= (current_date.at_beginning_of_year..current_date.at_end_of_year).map
    end

    def month_dates
      @month_dates ||= (current_date.at_beginning_of_month..current_date.at_end_of_month).map
    end

    def last_month_dates
      @last_month_dates ||= (last_month.at_beginning_of_month..last_month.at_end_of_month).map
    end

    def week_dates
      @week_dates ||= (current_date.at_beginning_of_week..current_date.at_end_of_week).map
    end

    def current_date
      @current_date ||= Date.current
    end

    def last_month
      @last_month ||= current_date - 1.month
    end
  end
end
