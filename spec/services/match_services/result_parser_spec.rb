# frozen_string_literal: true

require 'rails_helper'

describe MatchServices::ResultParser do
  let(:match) { create(:match) }

  let(:api_response) { double(body: json_response) }
  let(:response_parsed) { JSON.parse(api_response.body).with_indifferent_access }

  let(:match_periods) { response_parsed[:sport_event_status][:period_scores] }
  let(:mandatory_statistics) { response_parsed[:statistics][:teams].first[:statistics] }
  let(:visitor_statistics) { response_parsed[:statistics][:teams].last[:statistics] }

  let(:match_result) do
    {
      finished: true,
      mandatory_yellow_cards: mandatory_statistics[:yellow_cards],
      visitor_yellow_cards: visitor_statistics[:yellow_cards],
      mandatory_fouls: mandatory_statistics[:fouls],
      visitor_fouls: visitor_statistics[:fouls],
      mandatory_goals_first_period: match_periods.first[:home_score],
      visitor_goals_first_period: match_periods.first[:away_score],
      mandatory_goals_last_period: match_periods.last[:home_score],
      visitor_goals_last_period: match_periods.last[:away_score],
      ball_possession: ball_possession
    }
  end

  subject { described_class.execute(response_parsed) }

  context '.execute' do
    context 'when match is finished on API' do
      context 'and ball possesion is to visitor' do
        let(:ball_possession) { 2 }
        let(:json_response) { file_fixture('api-match-with-visitor-ballpossession.json').read }

        it 'return match result' do
          is_expected.to eq match_result
        end
      end

      context 'and ball possesion is to mandatory' do
        let(:ball_possession) { 1 }
        let(:json_response) { file_fixture('api-match-with-mandatory-ballpossession.json').read }

        it 'return match result' do
          is_expected.to eq match_result
        end
      end

      context 'and ball possesion is draw' do
        let(:ball_possession) { 0 }
        let(:json_response) { file_fixture('api-match-with-draw-ballpossession.json').read }

        it 'return match result' do
          is_expected.to eq match_result
        end
      end
    end
  end
end
