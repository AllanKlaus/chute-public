# frozen_string_literal: true

require 'rails_helper'

describe PointsServices::MonthReward do
  let(:number_of_rewards) { 1 }
  let(:last_month) { Date.current - 1.month }

  let(:user) { create(:user) }
  let(:match) { create(:match, match_date: last_month) }
  let(:hint) { create(:hint, match: match, user: user) }

  let!(:reward) { create(:reward, position: 1) }
  let!(:user_point) { create(:user_point, user: user, hint: hint) }

  let(:moviment) { Moviment.last }
  let(:user_datum) { user.user_datum }

  subject { described_class.execute }

  context 'when exists a user_datum' do
    it 'update user money on moviment and user_datum' do
      expect { subject }
        .to change { UserDatum.count }.by(number_of_rewards)
                                      .and change { Moviment.count }.by(number_of_rewards)

      expect(moviment.money).to eq reward.award
      expect(user_datum.money).to eq reward.award
    end
  end

  context 'when dont exists a user_datum' do
    let!(:user_data) { create(:user_datum, user: user, money: 10) }

    it 'update user money on moviment and user_datum' do
      expect { subject }.to not_change { UserDatum.count }
        .and change { Moviment.count }.by(number_of_rewards)
                                      .and change { user_data.reload.money }.by(reward.award)

      expect(moviment.money).to eq reward.award
    end
  end
end
