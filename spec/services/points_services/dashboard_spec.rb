# frozen_string_literal: true

require 'rails_helper'

describe PointsServices::Dashboard do
  let(:user) { create(:user) }
  let(:another_user) { create(:user) }
  let(:date) { Date.parse('28-May-10') }

  let(:last_year_points) { 40 }
  let(:year_points) { 30 }
  let(:month_points) { 20 }
  let(:week_points) { 10 }

  let(:total_points) { last_year_points + year_points + month_points + month_points + week_points }
  let(:total_year_points) { year_points + month_points + month_points + week_points }
  let(:total_month_points) { month_points + week_points }
  let(:total_last_month_points) { month_points }
  let(:total_week_points) { week_points }

  let(:dashboard) do
    {
      total_points: total_points,
      year_points: total_year_points,
      last_month_points: total_last_month_points,
      month_points: total_month_points,
      week_points: total_week_points
    }
  end

  let(:last_year_match) { create(:match, match_date: date.at_beginning_of_year - 1.month) }
  let(:last_year_hint) { create(:hint, match: last_year_match) }
  let(:year_match) { create(:match, match_date: date.at_beginning_of_year) }
  let(:year_hint) { create(:hint, match: year_match) }
  let(:last_month_match) { create(:match, match_date: date.at_beginning_of_month - 1.month) }
  let(:last_month_hint) { create(:hint, match: last_month_match) }
  let(:month_match) { create(:match, match_date: date.at_beginning_of_month) }
  let(:month_hint) { create(:hint, match: month_match) }
  let(:week_match) { create(:match, match_date: date.at_beginning_of_week) }
  let(:week_hint) { create(:hint, match: week_match) }

  before do
    allow(Date).to receive(:current) { date }
    create(:user_point, user: user, hint: last_year_hint, points: last_year_points)
    create(:user_point, user: user, hint: year_hint, points: year_points)
    create(:user_point, user: user, hint: last_month_hint, points: month_points)
    create(:user_point, user: user, hint: month_hint, points: month_points)
    create(:user_point, user: user, hint: week_hint, points: week_points)
    create(:user_point, user: another_user, hint: last_year_hint, points: last_year_points)
    create(:user_point, user: another_user, hint: year_hint, points: year_points)
    create(:user_point, user: another_user, hint: last_month_hint, points: month_points)
    create(:user_point, user: another_user, hint: month_hint, points: month_points)
    create(:user_point, user: another_user, hint: week_hint, points: week_points)
  end

  subject { described_class.execute(user) }

  it 'return dashboard from user' do
    is_expected.to eq(dashboard)
  end
end
