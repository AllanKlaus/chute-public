# frozen_string_literal: true

require 'rails_helper'

describe HintsServices::BasicCalculator do
  let(:params) do
    {
      hint: hint,
      team_points: team_points,
      all_points: all_points,
      type: type
    }
  end

  subject { described_class.execute(params) }

  context 'when calculate yellow card type' do
    let(:type) { :yellow_cards }
    let(:total_of_teams) { 2 }
    let(:total_yellow_cards) { yellow_cards * total_of_teams }
    let(:yellow_cards) { 3 }

    let(:match) do
      create(:match,
             mandatory_yellow_cards: yellow_cards,
             visitor_yellow_cards: yellow_cards)
    end

    let(:total_points) { all_points + assert_both_team_yellow_cards }
    let(:all_points) { 3 }
    let(:team_points) { 10 }
    let(:assert_both_team_yellow_cards) { total_of_teams * team_points }

    before do
      create(:point, name: 'all_yellow_cards', value: all_points)
      create(:point, name: 'team_yellow_cards', value: team_points)
    end

    context 'when user assert all_yellow_cards and team_yellow_cards' do
      let(:assert_points) { all_points + assert_both_team_yellow_cards }

      let(:hint) do
        create(:hint,
               match: match,
               mandatory_yellow_cards: yellow_cards,
               visitor_yellow_cards: yellow_cards)
      end

      it 'return points result' do
        is_expected.to eq(assert_points)
        is_expected.to eq(total_points)
      end
    end

    context 'when user assert all_yellow_cards' do
      let(:assert_points) { all_points }

      let(:hint) do
        create(:hint,
               match: match,
               mandatory_yellow_cards: total_yellow_cards,
               visitor_yellow_cards: 0)
      end

      it 'return points result' do
        is_expected.to eq(assert_points)
        is_expected.to_not eq(total_points)
      end
    end
  end

  context 'when calculate fouls type' do
    let(:type) { :fouls }
    let(:total_of_teams) { 2 }
    let(:total_fouls) { fouls * total_of_teams }
    let(:fouls) { 3 }

    let(:match) do
      create(:match,
             mandatory_fouls: fouls,
             visitor_fouls: fouls)
    end

    let(:total_points) { all_points + assert_both_team_fouls }
    let(:all_points) { 3 }
    let(:team_points) { 10 }
    let(:assert_both_team_fouls) { total_of_teams * team_points }

    before do
      create(:point, name: 'all_fouls', value: all_points)
      create(:point, name: 'team_fouls', value: team_points)
    end

    context 'when user assert all_fouls and team_fouls' do
      let(:assert_points) { all_points + assert_both_team_fouls }

      let(:hint) do
        create(:hint,
               match: match,
               mandatory_fouls: fouls,
               visitor_fouls: fouls)
      end

      it 'return points result' do
        is_expected.to eq(assert_points)
        is_expected.to eq(total_points)
      end
    end

    context 'when user assert all_fouls' do
      let(:assert_points) { all_points }

      let(:hint) do
        create(:hint,
               match: match,
               mandatory_fouls: total_fouls,
               visitor_fouls: 0)
      end

      it 'return points result' do
        is_expected.to eq(assert_points)
        is_expected.to_not eq(total_points)
      end
    end
  end
end
