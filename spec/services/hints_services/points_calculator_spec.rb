# frozen_string_literal: true

require 'rails_helper'

describe HintsServices::PointsCalculator do
  let(:hint_count) { 1 }
  let(:user_point) { UserPoint.last }

  let(:assert_all_goals) { 5 }
  let(:assert_scoreboard) { 70 }
  let(:assert_goals_per_period) { 2 }
  let(:assert_goals_in_all_periods) { 4 * assert_goals_per_period }
  let(:assert_winner) { 30 }
  let(:assert_team_fouls) { 10 }
  let(:assert_all_team_fouls) { 2 * assert_team_fouls }
  let(:assert_team_yellow_cards) { 10 }
  let(:assert_all_team_yellow_cards) { 2 * assert_team_yellow_cards }
  let(:assert_all_fouls) { 3 }
  let(:assert_all_yellow_cards) { 3 }

  let(:total_points) do
    assert_all_goals +
      assert_scoreboard +
      assert_goals_in_all_periods +
      assert_winner +
      assert_all_team_fouls +
      assert_all_team_yellow_cards +
      assert_all_fouls +
      assert_all_yellow_cards
  end

  before do
    create(:point, name: 'scoreboard', value: assert_scoreboard)
    create(:point, name: 'all_goals', value: assert_all_goals)
    create(:point, name: 'team_goals_per_period', value: assert_goals_per_period)
    create(:point, name: 'winner', value: assert_winner)
    create(:point, name: 'team_fouls', value: assert_team_fouls)
    create(:point, name: 'all_fouls', value: assert_all_fouls)
    create(:point, name: 'team_yellow_cards', value: assert_team_yellow_cards)
    create(:point, name: 'all_yellow_cards', value: assert_all_yellow_cards)
  end

  subject { described_class.execute(match_id: match.id) }

  context 'when assert everything' do
    let(:assert_points) do
      assert_all_goals +
        assert_scoreboard +
        assert_goals_in_all_periods +
        assert_winner +
        assert_all_team_fouls +
        assert_all_team_yellow_cards +
        assert_all_fouls +
        assert_all_yellow_cards
    end

    let(:goals_first_period) { 2 }
    let(:goals_last_period) { 3 }
    let(:yellow_cards) { 3 }
    let(:fouls) { 3 }

    let(:match) do
      create(:match,
             finished: true,
             mandatory_goals_first_period: goals_first_period,
             mandatory_goals_last_period: goals_last_period,
             visitor_goals_first_period: goals_first_period,
             visitor_goals_last_period: goals_last_period,
             mandatory_yellow_cards: yellow_cards,
             visitor_yellow_cards: yellow_cards,
             mandatory_fouls: fouls,
             visitor_fouls: fouls)
    end

    let!(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: goals_first_period,
             mandatory_goals_last_period: goals_last_period,
             visitor_goals_first_period: goals_first_period,
             visitor_goals_last_period: goals_last_period,
             mandatory_yellow_cards: yellow_cards,
             visitor_yellow_cards: yellow_cards,
             mandatory_fouls: fouls,
             visitor_fouls: fouls)
    end

    it 'create user_points' do
      expect { subject }.to change { UserPoint.count }.by(hint_count)
      expect(user_point.points).to eq(assert_points)
      expect(user_point.points).to eq(total_points)
    end
  end

  context 'when assert only fouls' do
    let(:assert_points) { assert_all_fouls + assert_all_team_fouls }

    let(:match_mandatory_goals_first_period) { 5 }
    let(:match_visitor_goals_first_period) { 2 }
    let(:match_goals_last_period) { 10 }
    let(:match_yellow_cards) { 3 }

    let(:hint_goals_first_period) { 3 }
    let(:hint_goals_last_period) { 2 }
    let(:hint_yellow_cards) { 0 }

    let(:fouls) { 3 }

    let(:match) do
      create(:match,
             finished: true,
             mandatory_goals_first_period: match_mandatory_goals_first_period,
             mandatory_goals_last_period: match_goals_last_period,
             visitor_goals_first_period: match_visitor_goals_first_period,
             visitor_goals_last_period: match_goals_last_period,
             mandatory_yellow_cards: match_yellow_cards,
             visitor_yellow_cards: match_yellow_cards,
             mandatory_fouls: fouls,
             visitor_fouls: fouls)
    end

    let!(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: hint_goals_first_period,
             mandatory_goals_last_period: hint_goals_last_period,
             visitor_goals_first_period: hint_goals_first_period,
             visitor_goals_last_period: hint_goals_last_period,
             mandatory_yellow_cards: hint_yellow_cards,
             visitor_yellow_cards: hint_yellow_cards,
             mandatory_fouls: fouls,
             visitor_fouls: fouls)
    end

    it 'create user_points' do
      expect { subject }.to change { UserPoint.count }.by(hint_count)
      expect(user_point.points).to eq(assert_points)
      expect(user_point.points).to_not eq(total_points)
    end
  end

  context 'when assert only yellow cards' do
    let(:assert_points) { assert_all_yellow_cards + assert_all_team_yellow_cards }

    let(:match_mandatory_goals_first_period) { 1 }
    let(:match_visitor_goals_first_period) { 1 }
    let(:match_mandatory_goals_last_period) { 1 }
    let(:match_visitor_goals_last_period) { 1 }
    let(:match_fouls) { 3 }

    let(:hint_mandatory_goals_first_period) { 3 }
    let(:hint_visitor_goals_first_period) { 2 }
    let(:hint_mandatory_goals_last_period) { 2 }
    let(:hint_visitor_goals_last_period) { 2 }
    let(:hint_fouls) { 0 }

    let(:yellow_cards) { 3 }

    let(:match) do
      create(:match,
             finished: true,
             mandatory_goals_first_period: match_mandatory_goals_first_period,
             mandatory_goals_last_period: match_mandatory_goals_last_period,
             visitor_goals_first_period: match_visitor_goals_first_period,
             visitor_goals_last_period: match_visitor_goals_last_period,
             mandatory_yellow_cards: yellow_cards,
             visitor_yellow_cards: yellow_cards,
             mandatory_fouls: match_fouls,
             visitor_fouls: match_fouls)
    end

    let!(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: hint_mandatory_goals_first_period,
             mandatory_goals_last_period: hint_mandatory_goals_last_period,
             visitor_goals_first_period: hint_visitor_goals_first_period,
             visitor_goals_last_period: hint_visitor_goals_last_period,
             mandatory_yellow_cards: yellow_cards,
             visitor_yellow_cards: yellow_cards,
             mandatory_fouls: hint_fouls,
             visitor_fouls: hint_fouls)
    end

    it 'create user_points' do
      expect { subject }.to change { UserPoint.count }.by(hint_count)
      expect(user_point.points).to eq(assert_points)
      expect(user_point.points).to_not eq(total_points)
    end
  end

  context 'when assert only winner' do
    let(:assert_points) { assert_winner }

    let(:match_mandatory_goals_first_period) { 7 }
    let(:match_visitor_goals_first_period) { 4 }
    let(:match_mandatory_goals_last_period) { 9 }
    let(:match_visitor_goals_last_period) { 4 }
    let(:match_yellow_cards) { 5 }
    let(:match_fouls) { 5 }

    let(:hint_mandatory_goals_first_period) { 8 }
    let(:hint_visitor_goals_first_period) { 5 }
    let(:hint_mandatory_goals_last_period) { 10 }
    let(:hint_visitor_goals_last_period) { 5 }
    let(:hint_yellow_cards) { 3 }
    let(:hint_fouls) { 3 }

    let(:match) do
      create(:match,
             finished: true,
             mandatory_goals_first_period: match_mandatory_goals_first_period,
             mandatory_goals_last_period: match_mandatory_goals_last_period,
             visitor_goals_first_period: match_visitor_goals_first_period,
             visitor_goals_last_period: match_visitor_goals_last_period,
             mandatory_yellow_cards: match_yellow_cards,
             visitor_yellow_cards: match_yellow_cards,
             mandatory_fouls: match_fouls,
             visitor_fouls: match_fouls)
    end

    let!(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: hint_mandatory_goals_first_period,
             mandatory_goals_last_period: hint_mandatory_goals_last_period,
             visitor_goals_first_period: hint_visitor_goals_first_period,
             visitor_goals_last_period: hint_visitor_goals_last_period,
             mandatory_yellow_cards: hint_yellow_cards,
             visitor_yellow_cards: hint_yellow_cards,
             mandatory_fouls: hint_fouls,
             visitor_fouls: hint_fouls)
    end

    it 'create user_points' do
      expect { subject }.to change { UserPoint.count }.by(hint_count)
      expect(user_point.points).to eq(assert_points)
      expect(user_point.points).to_not eq(total_points)
    end
  end

  context 'when assert only one team goal per period' do
    let(:assert_points) { assert_goals_per_period }

    let(:mandatory_goals_first_period) { 3 }

    let(:match_visitor_goals_first_period) { 4 }
    let(:match_mandatory_goals_last_period) { 1 }
    let(:match_visitor_goals_last_period) { 4 }
    let(:match_yellow_cards) { 5 }
    let(:match_fouls) { 5 }

    let(:hint_visitor_goals_first_period) { 5 }
    let(:hint_mandatory_goals_last_period) { 15 }
    let(:hint_visitor_goals_last_period) { 5 }
    let(:hint_yellow_cards) { 3 }
    let(:hint_fouls) { 3 }

    let(:match) do
      create(:match,
             finished: true,
             mandatory_goals_first_period: mandatory_goals_first_period,
             mandatory_goals_last_period: match_mandatory_goals_last_period,
             visitor_goals_first_period: match_visitor_goals_first_period,
             visitor_goals_last_period: match_visitor_goals_last_period,
             mandatory_yellow_cards: match_yellow_cards,
             visitor_yellow_cards: match_yellow_cards,
             mandatory_fouls: match_fouls,
             visitor_fouls: match_fouls)
    end

    let!(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: mandatory_goals_first_period,
             mandatory_goals_last_period: hint_mandatory_goals_last_period,
             visitor_goals_first_period: hint_visitor_goals_first_period,
             visitor_goals_last_period: hint_visitor_goals_last_period,
             mandatory_yellow_cards: hint_yellow_cards,
             visitor_yellow_cards: hint_yellow_cards,
             mandatory_fouls: hint_fouls,
             visitor_fouls: hint_fouls)
    end

    it 'create user_points' do
      expect { subject }.to change { UserPoint.count }.by(hint_count)
      expect(user_point.points).to eq(assert_points)
      expect(user_point.points).to_not eq(total_points)
    end
  end

  context 'when assert scoreboard' do
    let(:assert_points) { assert_scoreboard + assert_all_goals + assert_winner }

    let(:match_mandatory_goals_first_period) { 3 }
    let(:match_visitor_goals_first_period) { 3 }
    let(:match_mandatory_goals_last_period) { 4 }
    let(:match_visitor_goals_last_period) { 4 }
    let(:match_yellow_cards) { 5 }
    let(:match_fouls) { 5 }

    let(:hint_mandatory_goals_first_period) { 4 }
    let(:hint_visitor_goals_first_period) { 4 }
    let(:hint_mandatory_goals_last_period) { 3 }
    let(:hint_visitor_goals_last_period) { 3 }
    let(:hint_yellow_cards) { 3 }
    let(:hint_fouls) { 3 }

    let(:match) do
      create(:match,
             finished: true,
             mandatory_goals_first_period: match_mandatory_goals_first_period,
             mandatory_goals_last_period: match_mandatory_goals_last_period,
             visitor_goals_first_period: match_visitor_goals_first_period,
             visitor_goals_last_period: match_visitor_goals_last_period,
             mandatory_yellow_cards: match_yellow_cards,
             visitor_yellow_cards: match_yellow_cards,
             mandatory_fouls: match_fouls,
             visitor_fouls: match_fouls)
    end

    let!(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: hint_mandatory_goals_first_period,
             mandatory_goals_last_period: hint_mandatory_goals_last_period,
             visitor_goals_first_period: hint_visitor_goals_first_period,
             visitor_goals_last_period: hint_visitor_goals_last_period,
             mandatory_yellow_cards: hint_yellow_cards,
             visitor_yellow_cards: hint_yellow_cards,
             mandatory_fouls: hint_fouls,
             visitor_fouls: hint_fouls)
    end

    it 'create user_points' do
      expect { subject }.to change { UserPoint.count }.by(hint_count)
      expect(user_point.points).to eq(assert_points)
      expect(user_point.points).to_not eq(total_points)
    end
  end

  context 'when assert only all goals' do
    let(:assert_points) { assert_all_goals }

    let(:match_mandatory_goals_first_period) { 10 }
    let(:match_visitor_goals_first_period) { 0 }
    let(:match_mandatory_goals_last_period) { 0 }
    let(:match_visitor_goals_last_period) { 0 }
    let(:match_yellow_cards) { 5 }
    let(:match_fouls) { 5 }

    let(:hint_mandatory_goals_first_period) { 0 }
    let(:hint_visitor_goals_first_period) { 7 }
    let(:hint_mandatory_goals_last_period) { 2 }
    let(:hint_visitor_goals_last_period) { 1 }
    let(:hint_yellow_cards) { 3 }
    let(:hint_fouls) { 3 }

    let(:match) do
      create(:match,
             finished: true,
             mandatory_goals_first_period: match_mandatory_goals_first_period,
             mandatory_goals_last_period: match_mandatory_goals_last_period,
             visitor_goals_first_period: match_visitor_goals_first_period,
             visitor_goals_last_period: match_visitor_goals_last_period,
             mandatory_yellow_cards: match_yellow_cards,
             visitor_yellow_cards: match_yellow_cards,
             mandatory_fouls: match_fouls,
             visitor_fouls: match_fouls)
    end

    let!(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: hint_mandatory_goals_first_period,
             mandatory_goals_last_period: hint_mandatory_goals_last_period,
             visitor_goals_first_period: hint_visitor_goals_first_period,
             visitor_goals_last_period: hint_visitor_goals_last_period,
             mandatory_yellow_cards: hint_yellow_cards,
             visitor_yellow_cards: hint_yellow_cards,
             mandatory_fouls: hint_fouls,
             visitor_fouls: hint_fouls)
    end

    it 'create user_points' do
      expect { subject }.to change { UserPoint.count }.by(hint_count)
      expect(user_point.points).to eq(assert_points)
      expect(user_point.points).to_not eq(total_points)
    end
  end

  context 'when match is not finished' do
    let(:match) { create(:match, finished: false) }
    let!(:hint) { create(:hint, match: match) }

    it 'dont create user_points' do
      expect { subject }.to raise_error { I18n.t('errors.match_not_finished') }
    end
  end

  context 'when there is no hints' do
    let(:match) { create(:match, finished: true) }

    it 'dont create user_points' do
      expect { subject }.to_not change { UserPoint.count }
    end
  end
end
