# frozen_string_literal: true

require 'rails_helper'

describe HintsServices::Creator do
  let(:match) { create(:match) }
  let(:user) { create(:user) }

  let(:hint_params) do
    {
      mandatory_goals_first_period: 1,
      visitor_goals_first_period: 1,
      mandatory_goals_last_period: 1,
      visitor_goals_last_period: 1,
      mandatory_yellow_cards: 1,
      visitor_yellow_cards: 1,
      mandatory_fouls: 1,
      visitor_fouls: 1
    }
  end

  subject { described_class.execute(params: hint_params, match: match, user: user) }

  context 'when user do not give hint on this match' do
    let(:hints_to_create) { 1 }
    let(:hint) { Hint.last }

    context 'and match is enable to receive hints' do
      it 'create hint' do
        expect { subject }.to change { Hint.count }.by(hints_to_create)

        expect(hint.mandatory_goals_first_period).to eq(hint_params[:mandatory_goals_first_period])
        expect(hint.visitor_goals_first_period).to eq(hint_params[:visitor_goals_first_period])
        expect(hint.mandatory_goals_last_period).to eq(hint_params[:mandatory_goals_last_period])
        expect(hint.visitor_goals_last_period).to eq(hint_params[:visitor_goals_last_period])
        expect(hint.mandatory_yellow_cards).to eq(hint_params[:mandatory_yellow_cards])
        expect(hint.visitor_yellow_cards).to eq(hint_params[:visitor_yellow_cards])
        expect(hint.mandatory_fouls).to eq(hint_params[:mandatory_fouls])
        expect(hint.visitor_fouls).to eq(hint_params[:visitor_fouls])

        expect(hint.match).to eq(match)
        expect(hint.user).to eq(user)
      end
    end
  end

  context 'when user alredy give a hint on this match' do
    let!(:hint) { create(:hint, user: user, match: match) }
    it 'create hint' do
      expect { subject }.to_not change { Hint.count }
    end
  end

  context 'when match is finished' do
    context 'and finished status equal true' do
      let(:match) { create(:match, finished: true) }

      it 'create hint' do
        expect { subject }.to_not change { Hint.count }
      end
    end

    context 'and finished match_date equal current date' do
      let(:match) { create(:match, match_date: Date.current) }

      it 'create hint' do
        expect { subject }.to_not change { Hint.count }
      end
    end
  end
end
