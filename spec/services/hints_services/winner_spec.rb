# frozen_string_literal: true

require 'rails_helper'

describe HintsServices::Winner do
  subject { described_class.retrieve(match) }

  context 'when mandatory is the winner' do
    let(:match) { create(:match, mandatory_goals_first_period: 10) }
    let(:result) { :mandatory }

    it 'return mandatory' do
      is_expected.to eq(result)
    end
  end

  context 'when visitor is the winner' do
    let(:match) { create(:match, visitor_goals_first_period: 10) }
    let(:result) { :visitor }

    it 'return visitor' do
      is_expected.to eq(result)
    end
  end

  context 'when draw' do
    let(:match) { create(:match) }
    let(:result) { :draw }

    it 'return draw' do
      is_expected.to eq(result)
    end
  end
end
