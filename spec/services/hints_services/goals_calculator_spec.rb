# frozen_string_literal: true

require 'rails_helper'

describe HintsServices::GoalsCalculator do
  let(:total_goals) { goals_first_period + goals_last_period }
  let(:goals_first_period) { 2 }
  let(:goals_last_period) { 3 }

  let(:match) do
    create(:match,
           mandatory_goals_first_period: goals_first_period,
           mandatory_goals_last_period: goals_last_period,
           visitor_goals_first_period: goals_first_period,
           visitor_goals_last_period: goals_last_period)
  end

  let(:total_points) { assert_all_goals + assert_scoreboard + assert_goals_in_all_periods }
  let(:assert_all_goals) { 5 }
  let(:assert_scoreboard) { 70 }
  let(:assert_goals_per_period) { 2 }
  let(:assert_goals_in_all_periods) { 4 * assert_goals_per_period }

  before do
    create(:point, name: 'scoreboard', value: assert_scoreboard)
    create(:point, name: 'all_goals', value: assert_all_goals)
    create(:point, name: 'team_goals_per_period', value: assert_goals_per_period)
  end

  subject { described_class.execute(hint: hint) }

  context 'when user assert scoreboard, all goals and goals per period' do
    let(:assert_points) { assert_all_goals + assert_scoreboard + assert_goals_in_all_periods }

    let(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: goals_first_period,
             mandatory_goals_last_period: goals_last_period,
             visitor_goals_first_period: goals_first_period,
             visitor_goals_last_period: goals_last_period)
    end

    it 'return points result' do
      is_expected.to eq(assert_points)
      is_expected.to eq(total_points)
    end
  end

  context 'when user assert scoreboard and all goals' do
    let(:assert_points) { assert_all_goals + assert_scoreboard }
    let(:hint_goals_first_period) { goals_last_period }
    let(:hint_goals_last_period) { goals_first_period }

    let(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: hint_goals_first_period,
             mandatory_goals_last_period: hint_goals_last_period,
             visitor_goals_first_period: hint_goals_first_period,
             visitor_goals_last_period: hint_goals_last_period)
    end

    it 'return points result' do
      is_expected.to eq(assert_points)
      is_expected.to_not eq(total_points)
    end
  end

  context 'when user assert only all goals' do
    let(:assert_points) { assert_all_goals }
    let(:hint_goals_first_period) { goals_last_period * 2 }
    let(:hint_goals_last_period) { goals_first_period * 2 }

    let(:hint) do
      create(:hint,
             match: match,
             mandatory_goals_first_period: hint_goals_first_period,
             mandatory_goals_last_period: hint_goals_last_period,
             visitor_goals_first_period: 0,
             visitor_goals_last_period: 0)
    end

    it 'return points result' do
      is_expected.to eq(assert_points)
      is_expected.to_not eq(total_points)
    end
  end
end
