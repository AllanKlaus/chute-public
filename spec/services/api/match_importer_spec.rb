# frozen_string_literal: true

require 'rails_helper'

describe Api::MatchImporter do
  let(:season) { create(:season) }
  let(:first_match) { Match.first }
  let!(:mandatory) do
    create(:team,
           name: matches.first[:competitors].first[:name],
           team_code: matches.first[:competitors].first[:id],
           abbreviation: matches.first[:competitors].first[:abbreviation])
  end

  let(:visitor) do
    Team.find_by(team_code: matches.first[:competitors].last[:id])
  end

  let(:api_response) do
    JSON
      .parse(file_fixture('api-season-matches-request.json').read)
      .with_indifferent_access
  end

  let(:matches) { api_response[:sport_events] }

  subject { described_class.execute(season: season, matches: matches) }

  before { allow(Api::TeamCreator).to receive(:execute).and_call_original }

  context '.execute' do
    it 'create the matches' do
      expect { subject }.to change { Match.count }.by(matches.count)

      expect(first_match.match_code).to eq(matches.first[:id])
      expect(first_match.match_date).to eq(Date.parse(matches.first[:scheduled]))
      expect(first_match.season).to eq(season)
      expect(first_match.mandatory).to eq(mandatory)
      expect(first_match.visitor).to eq(visitor)

      expect(Api::TeamCreator).to have_received(:execute).at_least(:once)
    end
  end
end
