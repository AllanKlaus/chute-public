# frozen_string_literal: true

require 'rails_helper'

describe Api::MatchChecker do
  let(:yesterday) { Date.current - 1.day }
  let(:index) { 1 }

  subject { described_class.execute }

  before do
    allow(GetMatchResultJob).to receive(:set).with(wait: index.minutes).and_call_original
  end

  context 'when match is finished and not finished' do
    let!(:match) { create(:match, match_date: yesterday) }

    before { subject }

    it 'call worker' do
      expect(GetMatchResultJob).to have_received(:set).with(wait: index.minutes)
    end
  end

  context 'when match is finished and finished' do
    let!(:match) { create(:match, match_date: yesterday, finished: true) }

    before { subject }

    it 'dont call worker' do
      expect(GetMatchResultJob).to_not have_received(:set)
    end
  end

  context 'when match is not finished' do
    let(:today) { Date.current }
    let(:tomorrow) { Date.current + 1.day }
    let!(:match) { create(:match, match_date: today) }
    let!(:match_tomorrow) { create(:match, match_date: tomorrow) }

    before { subject }

    it 'dont call worker' do
      expect(GetMatchResultJob).to_not have_received(:set)
    end
  end
end
