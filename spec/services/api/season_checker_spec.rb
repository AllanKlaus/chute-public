# frozen_string_literal: true

require 'rails_helper'

describe Api::SeasonChecker do
  subject { described_class.execute }

  before do
    allow(ImportCurrentSeasonJob).to receive(:perform_now).with(season.id)

    subject
  end

  context 'when season is finished and active' do
    let(:season) { create(:season) }

    it 'call worker' do
      expect(ImportCurrentSeasonJob).to have_received(:perform_now)
        .with(season.id)
    end
  end

  context 'when season is finished and deactive' do
    let(:season) { create(:season, active: false) }

    it 'dont call worker' do
      expect(ImportCurrentSeasonJob).to_not have_received(:perform_now)
    end
  end

  context 'when season is not finished' do
    let(:tomorrow) { Date.current + 1.day }
    let(:season) { create(:season, end_date: tomorrow) }

    it 'dont call worker' do
      expect(ImportCurrentSeasonJob).to_not have_received(:perform_now)
    end
  end
end
