# frozen_string_literal: true

require 'rails_helper'

describe Api::SeasonImporter do
  let(:api_base_url) { 'API_URL' }
  let(:api_key) { '' }
  let(:tournament_code) { season.league.tournament_code }
  let(:continent) { season.league.continent }
  let(:route) { "/en/tournaments/#{tournament_code}/schedule.json?api_key=" }
  let(:number_of_new_seasons) { 1 }
  let(:api_route) { "#{api_base_url}#{continent}#{route}#{api_key}" }
  let!(:date_start_parsed) { Date.parse(api_season[:start_date]) }
  let!(:date_end_parsed) { Date.parse(api_season[:end_date]) }

  let(:api_response) { double(body: json_response) }
  let(:json_response) { file_fixture('api-season-matches-request.json').read }
  let(:response_parsed) { JSON.parse(api_response.body).with_indifferent_access }

  let(:season) do
    create(:season, start_date: season_start_date, end_date: season_end_date)
  end

  let(:api_season) { response_parsed[:sport_events].first[:season].symbolize_keys }

  subject { described_class.execute(season_id: season.id) }

  before do
    allow(Date).to receive(:parse)
      .with(api_season[:start_date]) { date_start_parsed }

    allow(Date).to receive(:parse)
      .with(api_season[:end_date]) { date_end_parsed }

    allow(Faraday).to receive(:get).with(api_route) { api_response }
  end

  context 'when there is a new season on api' do
    let(:season_start_date) { '2017-04-14' }
    let(:season_end_date) { '2017-12-16' }
    let(:number_of_new_seasons) { 1 }
    let(:new_season) { Season.last }

    let(:matches_response) { response_parsed[:sport_events] }
    let(:season_response) { matches_response.first[:season].symbolize_keys }

    it 'create new season' do
      expect(date_start_parsed).to receive(:eql?).with(season.start_date)
      expect(date_end_parsed).to receive(:eql?).with(season.end_date)

      expect { subject }
        .to change { Season.count }.by(number_of_new_seasons)
                                   .and change { Match.count }.by(matches_response.count)

      expect(Faraday).to have_received(:get).with(api_route)

      expect(new_season.matches).to eq(matches_response.count)
      expect(new_season.season_code).to eq(season_response[:id])
      expect(new_season.start_date).to eq(Date.parse(season_response[:start_date]))
      expect(new_season.end_date).to eq(Date.parse(season_response[:end_date]))
      expect(new_season.active).to be true
      expect(season.reload.active).to be false
    end
  end

  context 'when there is not a new season on api' do
    let(:season_start_date) { '2018-04-14' }
    let(:season_end_date) { '2018-12-16' }

    it 'dont create a new session' do
      expect { subject }.to_not change { Season.count }

      expect(Faraday).to have_received(:get).with(api_route)
      expect(season.reload.active).to be true
    end
  end
end
