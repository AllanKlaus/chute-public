# frozen_string_literal: true

require 'rails_helper'

describe Api::TeamCreator do
  let(:last_team) { Team.last }
  let(:teams_to_create) { 1 }
  let(:competitor) do
    {
      id: 'sr:competitor:1954',
      name: 'São Paulo',
      abbreviation: 'SPO'
    }
  end

  subject { described_class.execute(competitor) }

  context '.execute' do
    it 'create the matches' do
      expect { subject }.to change { Team.count }.by(teams_to_create)

      expect(last_team.name).to eq(competitor[:name])
      expect(last_team.team_code).to eq(competitor[:id])
      expect(last_team.abbreviation).to eq(competitor[:abbreviation])
    end
  end
end
