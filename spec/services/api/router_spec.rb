# frozen_string_literal: true

require 'rails_helper'

describe Api::Router do
  let(:match) { create(:match) }

  let(:api_base_url) { 'API_URL' }
  let(:api_key) { '' }
  let(:continent) { match.season.continent }
  let(:route) { "/en/matches/#{match.match_code}/summary.json?api_key=" }

  let(:api_route) { "#{api_base_url}#{continent}#{route}#{api_key}" }

  subject { described_class.execute(continent, route) }

  context '.execute' do
    it 'return mounted route' do
      is_expected.to eq api_route
    end
  end
end
