# frozen_string_literal: true

require 'rails_helper'

describe Api::MatchValidator do
  let(:match) { create(:match) }
  let(:api_base_url) { 'API_URL' }
  let(:api_key) { '' }
  let(:tournament_code) { match.season.tournament_code }
  let(:continent) { match.season.continent }
  let(:route) { "/en/matches/#{match.match_code}/summary.json?api_key=" }

  let(:api_response) { double(body: json_response) }
  let(:response_parsed) { JSON.parse(api_response.body).with_indifferent_access }

  let(:api_route) { "#{api_base_url}#{continent}#{route}#{api_key}" }

  let(:match_periods) do
    response_parsed[:sport_event_status][:period_scores]
  end

  let(:mandatory_statistics) do
    response_parsed[:statistics][:teams].first[:statistics]
  end

  let(:visitor_statistics) do
    response_parsed[:statistics][:teams].last[:statistics]
  end

  subject { described_class.execute(match_id: match.id) }

  before do
    allow(Faraday).to receive(:get).with(api_route) { api_response }
    allow(CalculateUserPointsJob).to receive(:perform_later).with(match.id)
  end

  context '.execute' do
    context 'when match is finished on API' do
      context 'and ball possesion is to visitor' do
        let(:ball_possession) { 'visitor' }

        let(:json_response) do
          file_fixture('api-match-with-visitor-ballpossession.json').read
        end

        it 'update match' do
          expect { subject }.to not_change { match.match_code }
            .and not_change { match.season_id }
            .and not_change { match.match_date }
            .and not_change { match.mandatory_id }
            .and not_change { match.visitor_id }

          match.reload

          expect(match.finished).to be true
          expect(match.mandatory_yellow_cards).to eq(mandatory_statistics[:yellow_cards])
          expect(match.visitor_yellow_cards).to eq(visitor_statistics[:yellow_cards])
          expect(match.mandatory_fouls).to eq(mandatory_statistics[:fouls])
          expect(match.visitor_fouls).to eq(visitor_statistics[:fouls])
          expect(match.ball_possession).to eq(ball_possession)
          expect(match.mandatory_goals_first_period).to eq(match_periods.first[:home_score])
          expect(match.visitor_goals_first_period).to eq(match_periods.first[:away_score])
          expect(match.mandatory_goals_last_period).to eq(match_periods.last[:home_score])
          expect(match.visitor_goals_last_period).to eq(match_periods.last[:away_score])

          expect(Faraday).to have_received(:get).with(api_route)
          expect(CalculateUserPointsJob).to have_received(:perform_later).with(match.id)
        end
      end

      context 'and ball possesion is to mandatory' do
        let(:ball_possession) { 'mandatory' }

        let(:json_response) do
          file_fixture('api-match-with-mandatory-ballpossession.json').read
        end

        it 'update match' do
          expect { subject }.to not_change { match.match_code }
            .and not_change { match.season_id }
            .and not_change { match.match_date }
            .and not_change { match.mandatory_id }
            .and not_change { match.visitor_id }

          match.reload

          expect(match.finished).to be true
          expect(match.mandatory_yellow_cards).to eq(mandatory_statistics[:yellow_cards])
          expect(match.visitor_yellow_cards).to eq(visitor_statistics[:yellow_cards])
          expect(match.mandatory_fouls).to eq(mandatory_statistics[:fouls])
          expect(match.visitor_fouls).to eq(visitor_statistics[:fouls])
          expect(match.ball_possession).to eq(ball_possession)
          expect(match.mandatory_goals_first_period).to eq(match_periods.first[:home_score])
          expect(match.visitor_goals_first_period).to eq(match_periods.first[:away_score])
          expect(match.mandatory_goals_last_period).to eq(match_periods.last[:home_score])
          expect(match.visitor_goals_last_period).to eq(match_periods.last[:away_score])

          expect(Faraday).to have_received(:get).with(api_route)
          expect(CalculateUserPointsJob).to have_received(:perform_later).with(match.id)
        end
      end

      context 'and ball possesion is draw' do
        let(:ball_possession) { 'draw' }

        let(:json_response) do
          file_fixture('api-match-with-draw-ballpossession.json').read
        end

        it 'update match' do
          expect { subject }.to not_change { match.match_code }
            .and not_change { match.season_id }
            .and not_change { match.match_date }
            .and not_change { match.mandatory_id }
            .and not_change { match.visitor_id }

          match.reload

          expect(match.finished).to be true
          expect(match.mandatory_yellow_cards).to eq(mandatory_statistics[:yellow_cards])
          expect(match.visitor_yellow_cards).to eq(visitor_statistics[:yellow_cards])
          expect(match.mandatory_fouls).to eq(mandatory_statistics[:fouls])
          expect(match.visitor_fouls).to eq(visitor_statistics[:fouls])
          expect(match.ball_possession).to eq(ball_possession)
          expect(match.mandatory_goals_first_period).to eq(match_periods.first[:home_score])
          expect(match.visitor_goals_first_period).to eq(match_periods.first[:away_score])
          expect(match.mandatory_goals_last_period).to eq(match_periods.last[:home_score])
          expect(match.visitor_goals_last_period).to eq(match_periods.last[:away_score])

          expect(Faraday).to have_received(:get).with(api_route)
          expect(CalculateUserPointsJob).to have_received(:perform_later).with(match.id)
        end
      end
    end

    context 'when match is not finished on API' do
      let(:json_response) do
        file_fixture('api-match-not-finished.json').read
      end

      it 'the matches' do
        expect { subject }.to not_change { match.reload }
        match.reload
        expect(Faraday).to have_received(:get).with(api_route)
        expect(CalculateUserPointsJob).to_not have_received(:perform_later).with(match.id)
      end
    end
  end
end
