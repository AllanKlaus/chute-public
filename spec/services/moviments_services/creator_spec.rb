# frozen_string_literal: true

require 'rails_helper'

describe MovimentsServices::Creator do
  let(:user) { create(:user) }
  let(:description) { 'Americanas' }
  let(:type) { :out }
  let(:params) { { rescue: money, store: description } }

  subject { described_class.execute(params: params, user: user) }

  context 'when user can make a moviment' do
    let!(:user_data) { create(:user_datum, money: money_int) }
    let(:money) { '50' }
    let(:money_int) { money.to_i }

    let(:expected_moviment) do
      {
        user: user,
        money: money_int,
        type: type,
        description: description
      }
    end

    before do
      allow(UserDatum).to receive(:find_by).with(user: user) { user_data }
      allow(Moviment).to receive(:create).with(expected_moviment)
    end

    it 'load moviments#show' do
      expect { subject }.to change { user_data.reload.money }.by(-money_int)

      expect(UserDatum).to have_received(:find_by).with(user: user) { user_data }
      expect(Moviment).to have_received(:create).with(expected_moviment)
    end
  end

  context 'when user can not make a moviment' do
    let!(:user_data) { create(:user_datum, money: money_int) }

    before do
      allow(UserDatum).to receive(:find_by).with(user: user) { user_data }
      allow(Moviment).to receive(:create)

      subject
    end

    context 'and money is less than minimum' do
      let(:money) { '30' }
      let(:money_int) { money.to_i }

      it 'load moviments#index' do
        subject

        expect(UserDatum).to have_received(:find_by).with(user: user) { user_data }
        expect(Moviment).to_not have_received(:create)
      end
    end

    context 'and money is more than user has' do
      let!(:user_data) { create(:user_datum, money: 50) }
      let(:money) { '60' }
      let(:money_int) { money.to_i }

      it 'load moviments#index' do
        subject

        expect(UserDatum).to have_received(:find_by).with(user: user) { user_data }
        expect(Moviment).to_not have_received(:create)
      end
    end
  end
end
