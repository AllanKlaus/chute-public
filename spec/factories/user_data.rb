# frozen_string_literal: true

FactoryBot.define do
  factory :user_datum do
    user
    name { generate(:user_name) }
    birth { Date.current + 2.days }
    money { '9.99' }
  end

  sequence :user_name do |n|
    "User #{n}#{FFaker::Name.first_name}"
  end
end
