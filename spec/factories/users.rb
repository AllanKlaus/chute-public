# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { generate(:user_name) }
    email { generate(:user_email) }
    encrypted_password { BCrypt::Password.create(FFaker::Internet.password(8)) }
    password { generate(:user_password) }
  end

  sequence :user_email do |n|
    "#{n}#{FFaker::Internet.email}"
  end

  sequence :user_password do |n|
    "#{n}#{FFaker::Internet.password(8)}"
  end
end
