# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    message { 'MyText' }
    contact
  end
end
