# frozen_string_literal: true

FactoryBot.define do
  factory :reward do
    position { 10 }
    award { 10 }
  end
end
