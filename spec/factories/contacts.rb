# frozen_string_literal: true

FactoryBot.define do
  factory :contact do
    subject { 'MyString' }
    email { 'email@email.com' }
    user
  end
end
