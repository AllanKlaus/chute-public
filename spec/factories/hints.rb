# frozen_string_literal: true

FactoryBot.define do
  factory :hint do
    user
    match
    mandatory_goals_first_period { 1 }
    visitor_goals_first_period { 1 }
    mandatory_goals_last_period { 1 }
    visitor_goals_last_period { 1 }
    mandatory_yellow_cards { 1 }
    visitor_yellow_cards { 1 }
    mandatory_fouls { 1 }
    visitor_fouls { 1 }
    ball_possession { 1 }
  end
end
