# frozen_string_literal: true

FactoryBot.define do
  factory :user_point do
    user
    hint
    points { 1 }
  end
end
