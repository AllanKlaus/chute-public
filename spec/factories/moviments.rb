# frozen_string_literal: true

FactoryBot.define do
  factory :moviment do
    user
    money { '9.99' }
    type { 1 }
  end
end
