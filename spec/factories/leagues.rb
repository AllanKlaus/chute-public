# frozen_string_literal: true

FactoryBot.define do
  factory :league do
    tournament_code { 'MyString' }
    name { 'MyString' }
    continent { 'am' }
  end
end
