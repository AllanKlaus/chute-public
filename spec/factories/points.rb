# frozen_string_literal: true

FactoryBot.define do
  factory :point do
    name { generate(:point_name) }
    value { 0 }
  end

  sequence :point_name do |n|
    "Team #{n}#{FFaker::Name.first_name}"
  end
end
