# frozen_string_literal: true

FactoryBot.define do
  factory :match do
    mandatory
    visitor
    season
    match_date { Date.current + 3.days }
    match_code { 'sr:match:123' }
  end
end
