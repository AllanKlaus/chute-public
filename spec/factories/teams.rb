# frozen_string_literal: true

FactoryBot.define do
  factory :team do
    name { generate(:team_name) }
    abbreviation { generate(:team_abbreviation) }
    team_code { generate(:team_code) }
  end

  factory :mandatory, class: Team do
    name { generate(:team_name) }
    abbreviation { generate(:team_abbreviation) }
    team_code { generate(:team_code) }
  end

  factory :visitor, class: Team do
    name { generate(:team_name) }
    abbreviation { generate(:team_abbreviation) }
    team_code { generate(:team_code) }
  end

  sequence :team_name do |n|
    "Team #{n}#{FFaker::Name.first_name}"
  end

  sequence :team_abbreviation do |n|
    "#{n}#{FFaker::UnitEnglish.area_abbr}".truncate(3)
  end

  sequence :team_code do |n|
    "#{n}#{FFaker::UnitEnglish.area_abbr}"
  end
end
