# frozen_string_literal: true

FactoryBot.define do
  factory :season do
    league
    season_code { 'MyString' }
    start_date { '2018-10-18' }
    end_date { '2018-10-19' }
    matches { 1 }
  end
end
