# frozen_string_literal: true

require 'rails_helper'

describe MatchesController, type: :controller do
  before do
    allow(controller).to receive(:authenticate_user!)
    allow(controller).to receive(:current_user)
  end

  describe '#index' do
    let(:to_create) { 15 }
    let!(:matches) { create_list(:match, to_create) }
    let!(:matches_finished) { create_list(:match, to_create, finished: true) }

    before do
      expect(Match).to receive(:this_week).and_call_original
      expect(Match).to receive(:not_ended).and_call_original
      process :index
    end

    it 'load matches#index' do
      expect(response.status).to be 200
    end
  end

  describe '#show' do
    let!(:match) { create(:match) }
    let(:params) { { id: match.id } }

    before do
      expect(Match).to receive(:find).with(match.id).and_call_original
      process :show, params: params
    end

    it 'load matches#show' do
      expect(response.status).to be 200
    end
  end

  describe '#ended' do
    let(:to_create) { 15 }
    let!(:matches) { create_list(:match, to_create) }
    let!(:matches_finished) { create_list(:match, to_create, finished: true) }

    before do
      expect(Match).to receive(:ended).and_call_original
      process :ended
    end

    it 'load matches#ended' do
      expect(response.status).to be 200
    end
  end
end
