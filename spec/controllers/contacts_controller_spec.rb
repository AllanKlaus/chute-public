# frozen_string_literal: true

require 'rails_helper'

describe ContactsController, type: :controller do
  let(:user) { create(:user) }

  describe '#index' do
    before do
      allow(controller).to receive(:authenticate_user!)
      allow(controller).to receive(:current_user) { user }

      expect(Contact).to receive(:where).with(user: user).and_call_original
      process :index
    end

    it 'load hints#index' do
      expect(response.status).to be 200
    end
  end

  describe '#show' do
    let(:contact) { create(:contact, user: user) }
    let(:params) { { id: contact.id.to_s } }

    before do
      allow(controller).to receive(:authenticate_user!)
      allow(controller).to receive(:current_user) { user }
    end

    context 'when success' do
      before do
        expect(Contact).to receive(:find_by!).with(user: user, id: params[:id])
        process :show, params: params
      end

      it 'load contact#answer' do
        expect(response.status).to be 200
      end
    end

    context 'when contact not found' do
      let(:contact) { create(:contact, user: create(:user)) }

      before do
        expect(Contact).to receive(:find_by!).with(user: user, id: params[:id]).and_call_original
        process :show, params: params
      end

      it 'load contact#new' do
        expect(response.status).to be 302
      end
    end
  end

  describe '#create' do
    let(:contact) { create(:contact) }

    let(:contact_expected_params) do
      {
        subject: params[:subject],
        email: params[:email],
        user: user
      }
    end

    let(:message_expected_params) do
      {
        message: params[:message],
        contact: contact
      }
    end

    let(:params) do
      {
        subject: 'subject',
        email: 'email',
        message: 'message',
        user: user
      }
    end

    before do
      create_list(:user, 10)

      expect(Contact).to receive(:create).with(contact_expected_params) { contact }
      expect(Message).to receive(:create).with(message_expected_params)
    end

    context 'when user is logged' do
      before do
        allow(controller).to receive(:current_user) { user }
        process :create, params: params
      end

      it 'load contact#create' do
        expect(response.status).to be 302
      end
    end

    context 'when user is not logged' do
      let(:user) { User.first }

      before do
        allow(controller).to receive(:current_user)
        process :create, params: params
      end

      it 'load contact#create' do
        expect(response.status).to be 302
      end
    end
  end

  describe '#new' do
    before do
      allow(controller).to receive(:current_user)

      process :new
    end

    it 'load contact#new' do
      expect(response.status).to be 200
    end
  end

  describe '#answer' do
    let(:contact) { create(:contact, user: user) }
    let(:params) { { id: contact.id.to_s } }

    before do
      allow(controller).to receive(:authenticate_user!)
      allow(controller).to receive(:current_user) { user }
    end

    context 'when success' do
      before do
        expect(Contact).to receive(:find_by!).with(user: user, id: params[:id])
        process :answer, params: params
      end

      it 'load contact#answer' do
        expect(response.status).to be 200
      end
    end

    context 'when contact not found' do
      let(:contact) { create(:contact, user: create(:user)) }

      before do
        expect(Contact).to receive(:find_by!).with(user: user, id: params[:id]).and_call_original
        process :answer, params: params
      end

      it 'load contact#new' do
        expect(response.status).to be 302
      end
    end
  end

  describe '#answer_message' do
    let(:contact) { create(:contact, user: user) }
    let(:params) { { id: contact.id.to_s, message: 'Message' } }

    before do
      allow(controller).to receive(:authenticate_user!)
      allow(controller).to receive(:current_user) { user }

      allow(Message).to receive(:create)
    end

    context 'when success' do
      before do
        expect(Contact).to receive(:find_by!).with(user: user, id: params[:id]) { contact }
        process :answer_message, params: params
      end

      it 'load contact#answer' do
        expect(response.status).to be 302
        expect(Message).to have_received(:create).with(contact: contact, message: params[:message])
      end
    end

    context 'when contact not found' do
      let(:contact) { create(:contact, user: create(:user)) }

      before do
        expect(Contact).to receive(:find_by!).with(user: user, id: params[:id]).and_call_original
        process :answer_message, params: params
      end

      it 'load contact#new' do
        expect(response.status).to be 302
        expect(Message).to_not have_received(:create)
      end
    end
  end
end
