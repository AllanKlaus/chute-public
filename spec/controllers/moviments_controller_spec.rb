# frozen_string_literal: true

require 'rails_helper'

describe MovimentsController, type: :controller do
  let(:user) { create(:user) }

  before do
    allow(controller).to receive(:authenticate_user!)
    allow(controller).to receive(:current_user) { user }
  end

  describe '#index' do
    before do
      expect(Moviment).to receive(:where).with(user: user).and_call_original
      expect(UserDatum).to receive(:find_or_create_by).with(user: user)

      process :index
    end

    it 'load moviments#index' do
      expect(response.status).to be 200
    end
  end

  describe '#create' do
    let(:description) { 'Americanas' }
    let(:params) { { rescue: money, store: description } }
    let(:type) { :out }

    context 'when user can make a moviment' do
      let!(:user_data) { create(:user_datum, money: money_int) }
      let(:money) { '50' }
      let(:money_int) { money.to_i }

      let(:expected_moviment) do
        {
          user: user,
          money: money_int,
          type: type,
          description: description
        }
      end

      before do
        expect(UserDatum).to receive(:find_by).with(user: user) { user_data }
        expect(Moviment).to receive(:create).with(expected_moviment)

        process :create, params: params
      end

      it 'load moviments#show' do
        expect(response.status).to be 302
      end
    end

    context 'when user can not make a moviment' do
      let!(:user_data) { create(:user_datum, money: money_int) }

      before do
        expect(UserDatum).to receive(:find_by).with(user: user) { user_data }
        allow(Moviment).to receive(:create)

        process :create, params: params
      end

      context 'and money is less than minimum' do
        let(:money) { '30' }
        let(:money_int) { money.to_i }

        it 'load moviments#index' do
          expect(response.status).to be 302
          expect(Moviment).to_not have_received(:create)
        end
      end

      context 'and money is more than user has' do
        let!(:user_data) { create(:user_datum, money: 50) }
        let(:money) { '60' }
        let(:money_int) { money.to_i }

        it 'load moviments#index' do
          expect(response.status).to be 302
          expect(Moviment).to_not have_received(:create)
        end
      end
    end
  end
end
