# frozen_string_literal: true

require 'rails_helper'

describe UsersController, type: :controller do
  describe '#dashboard' do
    let(:to_create) { 15 }
    let(:user) { create(:user) }
    let!(:matches) { create_list(:match, to_create) }
    let!(:matches_finished) { create_list(:match, to_create, finished: true) }

    before do
      allow(controller).to receive(:authenticate_user!)
      allow(controller).to receive(:current_user) { user }

      expect(PointsServices::Dashboard).to receive(:execute).with(user)
      expect(Match).to receive(:this_week).and_call_original
      expect(Match).to receive(:not_ended).and_call_original
      expect(user).to receive(:hints_count).and_call_original
      process :dashboard
    end

    it 'load user#dashboard' do
      expect(response.status).to be 200
    end
  end

  describe '#show' do
    let!(:user) { create(:user) }
    let(:params) { { id: user.slug } }

    before do
      allow(controller).to receive(:authenticate_user!)
      allow(controller).to receive(:current_user)

      expect(PointsServices::Dashboard).to receive(:execute).with(user)
      expect_any_instance_of(User).to receive(:hints_count).and_call_original
      expect_any_instance_of(Gravatar).to receive(:image_url)
      process :show, params: params
    end

    it 'load user#show' do
      expect(response.status).to be 200
    end
  end

  describe '#ranking' do
    let(:to_create) { 15 }
    let(:user) { create(:user) }
    let(:match) { create(:match, match_date: Date.current) }
    let(:hint) { create(:hint, match: match) }
    let(:user_point) { create(:user_point, hint: hint, user: user, points: 10) }
    let(:month_dates) { (Date.current.at_beginning_of_month..Date.current.at_end_of_month) }

    before do
      allow(controller).to receive(:authenticate_user!)
      allow(controller).to receive(:current_user)

      expect(PointsServices::Ranking).to receive(:execute).with(month_dates).and_call_original
      process :ranking
    end

    it 'load user#ranking' do
      expect(response.status).to be 200
    end
  end
end
