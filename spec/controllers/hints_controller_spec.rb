# frozen_string_literal: true

require 'rails_helper'

describe HintsController, type: :controller do
  let(:user) { create(:user) }

  before do
    allow(controller).to receive(:authenticate_user!)
    allow(controller).to receive(:current_user) { user }
  end

  describe '#index' do
    let!(:match) { create(:match) }

    before do
      expect(Hint).to receive(:by_user).with(user).and_call_original
      process :index
    end

    it 'load hints#index' do
      expect(response.status).to be 200
    end
  end

  describe '#show' do
    let(:params) { { id: hint.id } }
    let(:find_by) { { id: hint.id, user: user } }
    let!(:hint) { create(:hint) }

    before do
      expect(Hint).to receive(:find_by).with(find_by).and_call_original
      process :show, params: params
    end

    it 'load hints#show' do
      expect(response.status).to be 200
    end
  end

  describe '#new' do
    let!(:match) { create(:match) }
    let(:params) { { id: match.id } }

    before do
      expect(Match).to receive(:find).with(match.id).and_call_original
      process :new, params: params
    end

    it 'load hints#new' do
      expect(response.status).to be 200
    end
  end

  describe '#create' do
    let!(:match) { create(:match) }
    let(:params) { { match_id: match.id, hint: hint_params } }
    let(:action_controller_params) do
      ActionController::Parameters.new(hint_params)
                                  .permit(:mandatory_goals_first_period,
                                          :visitor_goals_first_period,
                                          :mandatory_goals_last_period,
                                          :visitor_goals_last_period,
                                          :mandatory_yellow_cards,
                                          :visitor_yellow_cards,
                                          :mandatory_fouls,
                                          :visitor_fouls)
    end

    let(:hint_params) do
      {
        mandatory_goals_first_period: '1',
        visitor_goals_first_period: '1',
        mandatory_goals_last_period: '1',
        visitor_goals_last_period: '1',
        mandatory_yellow_cards: '1',
        visitor_yellow_cards: '1',
        mandatory_fouls: '1',
        visitor_fouls: '1'
      }
    end

    before do
      expect(Match).to receive(:find).and_call_original
      expect(HintsServices::Creator).to receive(:execute)
        .with(params: action_controller_params, match: match, user: user)
        .and_call_original

      process :create, params: params
    end

    it 'load hints#create' do
      expect(response.status).to be 302
    end
  end
end
