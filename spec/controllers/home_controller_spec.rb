# frozen_string_literal: true

require 'rails_helper'

describe HomeController, type: :controller do
  before do
    allow(controller).to receive(:authenticate_user!)
    allow(controller).to receive(:current_user)
  end

  describe '#index' do
    let(:to_create) { 15 }
    let!(:matches) { create_list(:match, to_create) }
    let!(:matches_finished) { create_list(:match, to_create, finished: true) }

    before do
      expect(User).to receive(:count).and_call_original
      expect(Match).to receive(:ended).and_call_original
      expect(Hint).to receive(:count).and_call_original
      expect(Match).to receive(:not_ended).and_call_original
      expect(Match).to receive(:this_week).and_call_original
      expect(UserPoint).to receive(:all).and_call_original
      process :index
    end

    it 'load home#index' do
      expect(response.status).to be 200
    end
  end
end
