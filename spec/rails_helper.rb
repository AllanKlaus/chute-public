
require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)

abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'rspec/rails'
require 'simplecov'
# SimpleCov.start 'rails'
SimpleCov.start 'rails' do
  add_filter 'app/channels/application_cable/channel.rb'
  add_filter 'app/channels/application_cable/connection.rb'
  add_filter 'app/jobs/application_job.rb'
  add_filter 'app/mailers/application_mailer.rb'
  add_filter 'app/models/application_record.rb'
  add_filter 'app/helpers/application_helper.rb'
  add_filter 'app/controllers/users/confirmations_controller.rb'
  add_filter 'app/controllers/users/omniauth_callbacks_controller.rb'
  add_filter 'app/controllers/users/passwords_controller.rb'
  add_filter 'app/controllers/users/registrations_controller.rb'
  add_filter 'app/controllers/users/sessions_controller.rb'
  add_filter 'app/controllers/users/unlocks_controller.rb'
end

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec.configure do |config|
  config.include ActiveJob::TestHelper
  config.include FactoryBot::Syntax::Methods
  config.include ActiveSupport::Testing::TimeHelpers
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!
end

RSpec::Matchers.define_negated_matcher :not_change, :change