# frozen_string_literal: true

require 'rails_helper'

describe ApplicationHelper, type: :helper do
  describe '#match_text_status' do
    subject { helper.match_text_status(match) }

    context 'when match is finished' do
      let(:match) { create(:match, finished: true) }

      it 'returns text see_result' do
        is_expected.to eq(I18n.t('see_result'))
      end
    end

    context 'when match is not finished' do
      let(:match) { create(:match, finished: false) }

      it 'returns text do_hint' do
        is_expected.to eq(I18n.t('do_hint'))
      end
    end
  end
end
