# frozen_string_literal: true

require 'rails_helper'

describe MonthRewardJob do
  let(:job) { MonthRewardJob }
  let(:job_name) { 'MonthReward' }
  let(:job_queue) { :reward_jobs }

  it { is_expected.to be_processed_in job_queue }

  it 'enqueues MonthRewardJob' do
    expect { job.perform_later }.to enqueue_job(job)
  end

  context 'when call month reward job' do
    before { allow(PointsServices::MonthReward).to receive(:execute) }

    it 'call month reward service' do
      job.perform_now

      expect(PointsServices::MonthReward).to have_received(:execute)
    end
  end
end
