# frozen_string_literal: true

require 'rails_helper'

describe ImportCurrentSeasonJob do
  let(:job) { ImportCurrentSeasonJob }
  let(:job_name) { 'ImportCurrentSeason' }
  let(:job_queue) { :season_jobs }

  it { is_expected.to be_processed_in job_queue }

  it 'enqueues ImportCurrentSeasonJob' do
    expect { job.perform_later }.to enqueue_job(job)
  end

  context 'when there is a season finished' do
    let(:api_base_url) { 'API_URL' }
    let(:api_key) { '' }
    let(:tournament_code) { season.league.tournament_code }
    let(:continent) { season.league.continent }
    let(:route) { "/en/tournaments/#{tournament_code}/schedule.json?api_key=" }
    let(:number_of_new_seasons) { 1 }

    let(:api_response) { double(body: json_response) }
    let(:json_response) { file_fixture('api-season-matches-request.json').read }

    let(:api_route) { "#{api_base_url}#{continent}#{route}#{api_key}" }

    let(:season) do
      create(:season, start_date: '2017-10-18', end_date: '2017-10-18')
    end

    before do
      allow(Api::SeasonImporter).to receive(:execute)
        .with(season_id: season.id)
        .and_call_original

      allow(Faraday).to receive(:get).with(api_route) { api_response }
    end

    it 'create new season' do
      expect { job.perform_now(season.id) }
        .to change { Season.count }.by(number_of_new_seasons)

      expect(Api::SeasonImporter).to have_received(:execute)
        .with(season_id: season.id)
      expect(Faraday).to have_received(:get).with(api_route)
    end
  end
end
