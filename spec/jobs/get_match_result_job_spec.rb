# frozen_string_literal: true

require 'rails_helper'

describe GetMatchResultJob do
  let(:job) { GetMatchResultJob }
  let(:job_name) { 'GetMatchResult' }
  let(:job_queue) { :season_jobs }

  it { is_expected.to be_processed_in job_queue }

  it 'enqueues GetMatchResultJob' do
    expect { job.perform_later }.to enqueue_job(job)
  end

  context 'when there is a match finished' do
    let(:api_base_url) { 'API_URL' }
    let(:api_key) { '' }
    let(:tournament_code) { match.season.tournament_code }
    let(:continent) { match.season.continent }
    let(:route) { "/en/matches/#{match.match_code}/summary.json?api_key=" }

    let(:api_route) { "#{api_base_url}#{continent}#{route}#{api_key}" }
    let(:match) { create(:match) }
    let(:ball_possession) { 'visitor' }

    let(:api_response) { double(body: json_response) }
    let(:response_parsed) { JSON.parse(api_response.body).with_indifferent_access }

    let(:json_response) do
      file_fixture('api-match-with-visitor-ballpossession.json').read
    end

    let(:match_periods) do
      response_parsed[:sport_event_status][:period_scores]
    end

    let(:mandatory_statistics) do
      response_parsed[:statistics][:teams].first[:statistics]
    end

    let(:visitor_statistics) do
      response_parsed[:statistics][:teams].last[:statistics]
    end

    before do
      allow(Api::MatchValidator).to receive(:execute)
        .with(match_id: match.id)
        .and_call_original

      allow(Faraday).to receive(:get).with(api_route) { api_response }
    end

    it 'update match' do
      expect { job.perform_now(match.id) }
        .to not_change { match.match_code }
        .and not_change { match.season_id }
        .and not_change { match.match_date }
        .and not_change { match.mandatory_id }
        .and not_change { match.visitor_id }

      match.reload

      expect(match.mandatory_yellow_cards).to eq(mandatory_statistics[:yellow_cards])
      expect(match.visitor_yellow_cards).to eq(visitor_statistics[:yellow_cards])
      expect(match.mandatory_fouls).to eq(mandatory_statistics[:fouls])
      expect(match.visitor_fouls).to eq(visitor_statistics[:fouls])
      expect(match.ball_possession).to eq(ball_possession)
      expect(match.mandatory_goals_first_period).to eq(match_periods.first[:home_score])
      expect(match.visitor_goals_first_period).to eq(match_periods.first[:away_score])
      expect(match.mandatory_goals_last_period).to eq(match_periods.last[:home_score])
      expect(match.visitor_goals_last_period).to eq(match_periods.last[:away_score])

      expect(Api::MatchValidator).to have_received(:execute)
        .with(match_id: match.id)
      expect(Faraday).to have_received(:get).with(api_route)
    end
  end
end
