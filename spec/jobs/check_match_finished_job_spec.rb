# frozen_string_literal: true

require 'rails_helper'

describe CheckMatchFinishedJob do
  let(:job) { CheckMatchFinishedJob }
  let(:index) { 1 }
  let(:job_name) { 'CheckMatchFinished' }
  let(:job_queue) { :season_jobs }

  it { is_expected.to be_processed_in job_queue }

  it 'enqueues CheckMatchFinishedJob' do
    expect { job.perform_later }.to enqueue_job(job)
  end

  context 'when there is a match finished' do
    let(:yesterday) { Date.current - 1.day }
    let!(:match) { create(:match, match_date: yesterday) }

    before do
      allow(Api::MatchChecker).to receive(:execute).and_call_original
      allow(GetMatchResultJob).to receive(:set).with(wait: index.minutes).and_call_original

      job.perform_now
    end

    it 'call the worker that will check the external api' do
      expect(Api::MatchChecker).to have_received(:execute)
      expect(GetMatchResultJob).to have_received(:set).with(wait: index.minutes)
    end
  end

  context 'when there is a match not finished' do
    let!(:match) { create(:match, finished: true) }

    before do
      allow(Api::MatchChecker).to receive(:execute).and_call_original
      allow(GetMatchResultJob).to receive(:set)

      job.perform_now
    end

    it 'dont call any worker' do
      expect(Api::MatchChecker).to have_received(:execute)
      expect(GetMatchResultJob).to_not have_received(:set)
    end
  end
end
