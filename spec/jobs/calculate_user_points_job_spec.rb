# frozen_string_literal: true

require 'rails_helper'

describe CalculateUserPointsJob do
  let(:job) { CalculateUserPointsJob }
  let(:job_name) { 'CalculateUserPoints' }
  let(:job_queue) { :hint_jobs }

  it { is_expected.to be_processed_in job_queue }

  it 'enqueues CalculateUserPointsJob' do
    expect { job.perform_later }.to enqueue_job(job)
  end

  context 'when there is a match finished' do
    let(:match) { create(:match) }

    before do
      allow(HintsServices::PointsCalculator).to receive(:execute).with(match_id: match.id)

      job.perform_now(match.id)
    end

    it 'update match' do
      expect(HintsServices::PointsCalculator).to have_received(:execute).with(match_id: match.id)
    end
  end
end
