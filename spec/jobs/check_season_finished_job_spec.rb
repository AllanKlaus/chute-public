# frozen_string_literal: true

require 'rails_helper'

describe CheckSeasonFinishedJob do
  let(:job) { CheckSeasonFinishedJob }
  let(:job_name) { 'CheckSeasonFinished' }
  let(:job_queue) { :season_jobs }

  it { is_expected.to be_processed_in job_queue }

  it 'enqueues CheckSeasonFinishedJob' do
    expect { job.perform_later }.to enqueue_job(job)
  end

  context 'when there is a season finished' do
    let(:season) { create(:season) }

    before do
      allow(Api::SeasonChecker).to receive(:execute).and_call_original
      allow(ImportCurrentSeasonJob).to receive(:perform_now).with(season.id)

      job.perform_now
    end

    it 'call the worker that will check the external api' do
      expect(Api::SeasonChecker).to have_received(:execute)
      expect(ImportCurrentSeasonJob).to have_received(:perform_now)
        .with(season.id)
    end
  end

  context 'when there is a season not finished' do
    let(:tomorrow) { Date.current + 1.day }
    let(:season) { create(:season, end_date: tomorrow) }

    before do
      allow(Api::SeasonChecker).to receive(:execute).and_call_original
      allow(ImportCurrentSeasonJob).to receive(:perform_now)

      job.perform_now
    end

    it 'dont call any worker' do
      expect(Api::SeasonChecker).to have_received(:execute)
      expect(ImportCurrentSeasonJob).to_not have_received(:perform_now)
    end
  end
end
