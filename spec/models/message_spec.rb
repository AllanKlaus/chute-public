# frozen_string_literal: true

require 'rails_helper'

describe Message do
  let!(:message) { create(:message) }

  describe '#contact' do
    subject { message.contact }

    it 'return user' do
      is_expected.to be_a Contact
    end
  end
end
