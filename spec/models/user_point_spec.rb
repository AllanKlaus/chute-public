# frozen_string_literal: true

require 'rails_helper'

describe UserPoint do
  let(:user_point) { create(:user_point) }

  describe '.user' do
    subject { user_point.user }

    it 'return league team' do
      is_expected.to be_a User
    end
  end

  describe '.hint' do
    subject { user_point.hint }

    it 'return league team' do
      is_expected.to be_a Hint
    end
  end

  describe '.match_date' do
    subject { user_point.match_date }

    it 'return league team' do
      is_expected.to eq user_point.hint.match_date
    end
  end
end
