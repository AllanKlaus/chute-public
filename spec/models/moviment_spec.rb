# frozen_string_literal: true

require 'rails_helper'

describe Moviment do
  let(:moviment) { create(:moviment) }

  describe '.user' do
    subject { moviment.user }

    it 'return user' do
      is_expected.to be_a User
    end
  end

  describe '.type' do
    subject { moviment.type }

    context 'when type is out' do
      let(:type) { 'out' }
      let(:moviment) { create(:moviment, type: 1) }

      it 'return type out' do
        is_expected.to eq(type)
      end
    end

    context 'when type is in' do
      let(:type) { 'in' }
      let(:moviment) { create(:moviment, type: 0) }

      it 'return type in' do
        is_expected.to eq(type)
      end
    end
  end
end
