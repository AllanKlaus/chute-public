# frozen_string_literal: true

require 'rails_helper'

describe Contact do
  let!(:contact) { create(:contact) }

  describe '#user' do
    subject { contact.user }

    it 'return user' do
      is_expected.to be_a User
    end
  end

  describe '#messages' do
    let!(:message) { create(:message, contact: contact) }
    subject { contact.messages.first }

    it 'return message' do
      is_expected.to be_a Message
    end
  end
end
