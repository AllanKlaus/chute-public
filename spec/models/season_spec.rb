# frozen_string_literal: true

require 'rails_helper'

describe Season do
  let(:season) { create(:season) }

  describe '#league' do
    subject { season.league }

    it 'return league team' do
      is_expected.to be_a League
    end
  end

  describe '#tournament_code' do
    subject { season.tournament_code }

    it 'return tournament_code team' do
      is_expected.to eq(season.league.tournament_code)
    end
  end

  describe '#continent' do
    subject { season.continent }

    it 'return continent team' do
      is_expected.to eq(season.league.continent)
    end
  end

  describe '.to_check' do
    let(:tomorrow) { Date.current + 1.day }
    let(:season_not_finished) { create(:season, end_date: tomorrow) }
    let(:season_finished_and_deactive) { create(:season, active: false) }

    subject { described_class.to_check }

    it 'return seasons that need to be updated' do
      is_expected.to include(season)
      is_expected.to_not include(season_not_finished)
      is_expected.to_not include(season_finished_and_deactive)
    end
  end

  describe '#league_name' do
    subject { season.league_name }

    it 'return tournament_code team' do
      is_expected.to eq(season.league.name)
    end
  end
end
