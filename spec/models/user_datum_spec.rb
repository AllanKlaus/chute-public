# frozen_string_literal: true

require 'rails_helper'

describe UserDatum do
  let(:user_datum) { create(:user_datum) }

  describe '.user' do
    subject { user_datum.user }

    it 'return league team' do
      is_expected.to be_a User
    end
  end
end
