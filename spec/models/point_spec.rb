# frozen_string_literal: true

require 'rails_helper'

describe Point do
  describe '.scoreboard' do
    let(:point_value) { 70 }
    before { create(:point, name: 'scoreboard', value: point_value) }
    subject { described_class.scoreboard }

    it 'return point value for scoreboard' do
      is_expected.to eq point_value
    end
  end

  describe '.winner' do
    let(:point_value) { 30 }
    before { create(:point, name: 'winner', value: point_value) }
    subject { described_class.winner }

    it 'return point value for winner' do
      is_expected.to eq point_value
    end
  end

  describe '.team_fouls' do
    let(:point_value) { 10 }
    before { create(:point, name: 'team_fouls', value: point_value) }
    subject { described_class.team_fouls }

    it 'return point value for team_fouls' do
      is_expected.to eq point_value
    end
  end

  describe '.team_yellow_cards' do
    let(:point_value) { 10 }
    before { create(:point, name: 'team_yellow_cards', value: point_value) }
    subject { described_class.team_yellow_cards }

    it 'return point value for team_yellow_cards' do
      is_expected.to eq point_value
    end
  end

  describe '.all_goals' do
    let(:point_value) { 5 }
    before { create(:point, name: 'all_goals', value: point_value) }
    subject { described_class.all_goals }

    it 'return point value for all_goals' do
      is_expected.to eq point_value
    end
  end

  describe '.all_fouls' do
    let(:point_value) { 3 }
    before { create(:point, name: 'all_fouls', value: point_value) }
    subject { described_class.all_fouls }

    it 'return point value for all_fouls' do
      is_expected.to eq point_value
    end
  end

  describe '.all_yellow_cards' do
    let(:point_value) { 3 }
    before { create(:point, name: 'all_yellow_cards', value: point_value) }
    subject { described_class.all_yellow_cards }

    it 'return point value for all_yellow_cards' do
      is_expected.to eq point_value
    end
  end

  describe '.team_goals_per_period' do
    let(:point_value) { 2 }
    before { create(:point, name: 'team_goals_per_period', value: point_value) }
    subject { described_class.team_goals_per_period }

    it 'return point value for team_goals_per_period' do
      is_expected.to eq point_value
    end
  end

  describe '.ball_possession' do
    let(:point_value) { 1 }
    before { create(:point, name: 'ball_possession', value: point_value) }
    subject { described_class.ball_possession }

    it 'return point value for ball_possession' do
      is_expected.to eq point_value
    end
  end
end
