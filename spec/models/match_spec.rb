# frozen_string_literal: true

require 'rails_helper'

describe Match do
  let!(:match) { create(:match) }

  describe '.mandatory' do
    subject { match.mandatory }

    it 'return mandatory team' do
      is_expected.to be_a Team
    end
  end

  describe '.visitor' do
    subject { match.visitor }

    it 'return visitor team' do
      is_expected.to be_a Team
    end
  end

  describe '.season' do
    subject { match.season }

    it 'return season match' do
      is_expected.to be_a Season
    end
  end

  describe '.ball_possession' do
    subject { match.ball_possession }

    context 'when ball_possession is draw' do
      let(:ball_possession_result) { 'draw' }
      let(:match) { create(:match, ball_possession: 0) }

      it 'return season match' do
        is_expected.to eq(ball_possession_result)
      end
    end

    context 'when ball_possession is greater to mandatory' do
      let(:ball_possession_result) { 'mandatory' }
      let(:match) { create(:match, ball_possession: 1) }

      it 'return season match' do
        is_expected.to eq(ball_possession_result)
      end
    end

    context 'when ball_possession is greater to visitor' do
      let(:ball_possession_result) { 'visitor' }
      let(:match) { create(:match, ball_possession: 2) }

      it 'return season match' do
        is_expected.to eq(ball_possession_result)
      end
    end
  end

  describe '.continent' do
    let(:continent) { 'am' }
    subject { match.continent }

    it 'return continent of match' do
      is_expected.to eq(continent)
    end
  end

  describe '.to_check' do
    let(:yesterday) { Date.current - 1.day }
    let(:today) { Date.current }
    let(:tomorrow) { Date.current + 1.day }
    let(:match_today) { create(:match, match_date: today) }
    let(:match_yesterday_not_finished) { create(:match, match_date: yesterday) }
    let(:match_yesterday_finished) { create(:match, match_date: yesterday, finished: true) }
    let(:match_tomorrow) { create(:match, match_date: tomorrow) }

    subject { described_class.to_check }

    it 'return matches that need to be updated' do
      is_expected.to include(match_yesterday_not_finished)
      is_expected.to_not include(match_yesterday_finished)
      is_expected.to_not include(match_tomorrow)
      is_expected.to_not include(match_today)
    end
  end

  describe '.ended' do
    let(:finished_match) { create(:match, finished: true) }
    subject { described_class.ended }

    it 'return matches that is finished' do
      is_expected.to include(finished_match)
      is_expected.to_not include(match)
    end
  end

  describe '.not_ended' do
    let(:finished_match) { create(:match, finished: true) }
    subject { described_class.not_ended }

    it 'return matches that is not finished' do
      is_expected.to_not include(finished_match)
      is_expected.to include(match)
    end
  end

  describe '.mandatory_goals' do
    let(:total_goals) { goals_first_period + goals_last_period }
    let(:goals_first_period) { 2 }
    let(:goals_last_period) { 3 }

    let(:match) do
      create(:match,
             mandatory_goals_first_period: goals_first_period,
             mandatory_goals_last_period: goals_last_period)
    end
    subject { match.mandatory_goals }

    it 'return matches that is finished' do
      is_expected.to eq(total_goals)
    end
  end

  describe '.visitor_goals' do
    let(:total_goals) { goals_first_period + goals_last_period }
    let(:goals_first_period) { 2 }
    let(:goals_last_period) { 3 }

    let(:match) do
      create(:match,
             visitor_goals_first_period: goals_first_period,
             visitor_goals_last_period: goals_last_period)
    end
    subject { match.visitor_goals }

    it 'return matches that is finished' do
      is_expected.to eq(total_goals)
    end
  end

  describe '.this_week' do
    let!(:week_match) { create(:match, match_date: Date.current.at_beginning_of_week + 1.day) }
    let!(:last_week_match) { create(:match, match_date: Date.current - 1.week) }
    let!(:next_week_match) { create(:match, match_date: Date.current + 1.week) }

    subject { described_class.this_week }

    it 'return matches of the week' do
      is_expected.to include(week_match)
      is_expected.to_not include(last_week_match)
      is_expected.to_not include(next_week_match)
    end

    context 'when there is more than one match to start' do
      let!(:match) { create(:match, match_date: Date.current.at_beginning_of_week) }
      let!(:last_match) { create(:match, match_date: Date.current.at_end_of_week) }

      it 'order matchs by match_date' do
        expect(subject.first).to eq(match)
        expect(subject.last).to eq(last_match)
      end
    end
  end

  describe '.mandatory_name' do
    subject { match.mandatory_name }

    it 'return matches that is not finished' do
      is_expected.to eq(match.mandatory.name)
    end
  end

  describe '.visitor_name' do
    subject { match.visitor_name }

    it 'return matches that is not finished' do
      is_expected.to eq(match.visitor.name)
    end
  end

  describe '.finished?' do
    subject { match.finished? }

    context 'when match is finished' do
      context 'and finished status equal true' do
        let(:match_finished_status) { true }
        let(:match) { create(:match, finished: match_finished_status) }

        it 'return true' do
          is_expected.to be match_finished_status
        end
      end

      context 'and finished status equal true' do
        let(:match_finished_status) { true }
        let(:match) { create(:match, match_date: Date.current) }

        it 'return true' do
          is_expected.to be match_finished_status
        end
      end
    end

    context 'when match is not finished' do
      let(:match_finished_status) { false }
      let(:match) { create(:match, finished: match_finished_status) }

      it 'return false' do
        is_expected.to be match_finished_status
      end
    end
  end

  describe '.league_name' do
    subject { match.league_name }

    it 'return matches that is not finished' do
      is_expected.to eq(match.season.league_name)
    end
  end

  describe '.winner' do
    subject { match.winner }

    context 'when mandatory is the winner' do
      let!(:match) { create(:match, mandatory_goals_first_period: 1) }
      let(:result) { :mandatory }

      it 'return mandatory as winner' do
        is_expected.to eq(result)
      end
    end

    context 'when visitor is the winner' do
      let!(:match) { create(:match, visitor_goals_first_period: 1) }
      let(:result) { :visitor }

      it 'return visitor as winner' do
        is_expected.to eq(result)
      end
    end

    context 'when draw' do
      let(:result) { :draw }

      it 'return draw' do
        is_expected.to eq(result)
      end
    end
  end
end
