# frozen_string_literal: true

require 'rails_helper'

describe Hint do
  let!(:hint) { create(:hint) }

  describe '.user' do
    subject { hint.user }

    it 'return user' do
      is_expected.to be_a User
    end
  end

  describe '.match' do
    subject { hint.match }

    it 'return match' do
      is_expected.to be_a Match
    end
  end

  describe '.by_user' do
    let(:user) { create(:user) }
    let!(:user_hint) { create(:hint, user: user) }

    subject { described_class.by_user(user) }

    it 'return hints from user' do
      expect(subject.first).to eq(user_hint)
      expect(subject.first).to_not eq(hint)
    end
  end

  describe '.mandatory_name' do
    subject { hint.mandatory_name }

    it 'return mandatory_name' do
      is_expected.to eq(hint.match.mandatory_name)
    end
  end

  describe '.visitor_name' do
    subject { hint.visitor_name }

    it 'return visitor_name' do
      is_expected.to eq(hint.match.visitor_name)
    end
  end

  describe '.match_date' do
    subject { hint.match_date }

    it 'return match_date' do
      is_expected.to eq(hint.match.match_date)
    end
  end

  describe '.league_name' do
    subject { hint.league_name }

    it 'return league_name' do
      is_expected.to eq(hint.match.league_name)
    end
  end

  describe '.mandatory_goals' do
    let(:goals_first_period) { 1 }
    let(:goals_last_period) { 2 }
    let(:mandatory_goals) { hint.mandatory_goals_first_period + hint.mandatory_goals_last_period }

    let(:hint) do
      create(:hint,
             mandatory_goals_first_period: goals_first_period,
             mandatory_goals_last_period: goals_last_period)
    end

    subject { hint.mandatory_goals }

    it 'return mandatory_goals' do
      is_expected.to eq(mandatory_goals)
    end
  end

  describe '.visitor_goals' do
    let(:goals_first_period) { 1 }
    let(:goals_last_period) { 2 }
    let(:visitor_goals) { hint.visitor_goals_first_period + hint.visitor_goals_last_period }

    let(:hint) do
      create(:hint,
             visitor_goals_first_period: goals_first_period,
             visitor_goals_last_period: goals_last_period)
    end

    subject { hint.visitor_goals }

    it 'return visitor_goals' do
      is_expected.to eq(visitor_goals)
    end
  end

  describe '.match_mandatory_goals_first_period' do
    let(:match) { create(:match, mandatory_goals_first_period: 1) }
    let(:hint) { create(:hint, match: match) }

    subject { hint.match_mandatory_goals_first_period }

    it 'return match mandatory_goals_first_period' do
      is_expected.to eq(match.mandatory_goals_first_period)
    end
  end

  describe '.match_mandatory_goals_last_period' do
    let(:match) { create(:match, mandatory_goals_last_period: 1) }
    let(:hint) { create(:hint, match: match) }

    subject { hint.match_mandatory_goals_last_period }

    it 'return match mandatory_goals_last_period' do
      is_expected.to eq(match.mandatory_goals_last_period)
    end
  end

  describe '.match_visitor_goals_first_period' do
    let(:match) { create(:match, visitor_goals_first_period: 1) }
    let(:hint) { create(:hint, match: match) }

    subject { hint.match_visitor_goals_first_period }

    it 'return match visitor_goals_first_period' do
      is_expected.to eq(match.visitor_goals_first_period)
    end
  end

  describe '.match_visitor_goals_last_period' do
    let(:match) { create(:match, visitor_goals_last_period: 1) }
    let(:hint) { create(:hint, match: match) }

    subject { hint.match_visitor_goals_last_period }

    it 'return match visitor_goals_last_period' do
      is_expected.to eq(match.visitor_goals_last_period)
    end
  end

  describe '.match_mandatory_yellow_cards' do
    let(:match) { create(:match, mandatory_yellow_cards: 1) }
    let(:hint) { create(:hint, match: match) }

    subject { hint.match_mandatory_yellow_cards }

    it 'return match mandatory_yellow_cards' do
      is_expected.to eq(match.mandatory_yellow_cards)
    end
  end

  describe '.match_visitor_yellow_cards' do
    let(:match) { create(:match, visitor_yellow_cards: 1) }
    let(:hint) { create(:hint, match: match) }

    subject { hint.match_visitor_yellow_cards }

    it 'return match visitor_yellow_cards' do
      is_expected.to eq(match.visitor_yellow_cards)
    end
  end

  describe '.match_mandatory_fouls' do
    let(:match) { create(:match, mandatory_fouls: 1) }
    let(:hint) { create(:hint, match: match) }

    subject { hint.match_mandatory_fouls }

    it 'return match mandatory_fouls' do
      is_expected.to eq(match.mandatory_fouls)
    end
  end

  describe '.match_visitor_fouls' do
    let(:match) { create(:match, visitor_fouls: 1) }
    let(:hint) { create(:hint, match: match) }

    subject { hint.match_visitor_fouls }

    it 'return match visitor_fouls' do
      is_expected.to eq(match.visitor_fouls)
    end
  end

  describe '.match_finished?' do
    let(:hint) { create(:hint, match: match) }

    subject { hint.match_finished? }

    context 'when match is finished' do
      let(:match_finished) { true }
      let(:match) { create(:match, finished: match_finished) }

      it 'return is finished' do
        is_expected.to eq(match_finished)
      end
    end

    context 'when match is not finished' do
      let(:match_finished) { false }
      let(:match) { create(:match, finished: match_finished) }

      it 'return is not finished' do
        is_expected.to eq(match_finished)
      end
    end
  end

  describe '.match_mandatory_goals' do
    let(:total_goals) { goals_first_period + goals_last_period }
    let(:goals_first_period) { 2 }
    let(:goals_last_period) { 3 }

    let(:match) do
      create(:match,
             mandatory_goals_first_period: goals_first_period,
             mandatory_goals_last_period: goals_last_period)
    end

    let(:hint) { create(:hint, match: match) }

    subject { hint.match_mandatory_goals }

    it 'return match mandatory_goals' do
      is_expected.to eq(total_goals)
    end
  end

  describe '.match_mandatory_goals' do
    let(:total_goals) { goals_first_period + goals_last_period }
    let(:goals_first_period) { 2 }
    let(:goals_last_period) { 3 }

    let(:match) do
      create(:match,
             visitor_goals_first_period: goals_first_period,
             visitor_goals_last_period: goals_last_period)
    end

    let(:hint) { create(:hint, match: match) }

    subject { hint.match_visitor_goals }

    it 'return match visitor_goals' do
      is_expected.to eq(total_goals)
    end
  end

  describe '.winner' do
    subject { hint.winner }

    context 'when mandatory is the winner' do
      let!(:hint) { create(:hint, mandatory_goals_first_period: 7) }
      let(:result) { :mandatory }

      it 'return mandatory as winner' do
        is_expected.to eq(result)
      end
    end

    context 'when visitor is the winner' do
      let!(:hint) { create(:hint, visitor_goals_first_period: 7) }
      let(:result) { :visitor }

      it 'return visitor as winner' do
        is_expected.to eq(result)
      end
    end

    context 'when draw' do
      let(:result) { :draw }

      it 'return draw' do
        is_expected.to eq(result)
      end
    end
  end

  describe '.match_winner' do
    let(:result) { :visitor }
    let(:goals_first_period) { 2 }
    let(:goals_last_period) { 3 }

    let(:match) do
      create(:match,
             visitor_goals_first_period: goals_first_period,
             visitor_goals_last_period: goals_last_period)
    end

    let(:hint) { create(:hint, match: match) }

    subject { hint.match_winner }

    it 'return match winner' do
      is_expected.to eq(result)
    end
  end

  describe '.hint_type_team' do
    let(:mandatory_fouls) { 5 }
    let(:visitor_fouls) { 10 }

    let(:mandatory_yellow_cards) { 10 }
    let(:visitor_yellow_cards) { 5 }

    let(:hint) do
      create(:hint,
             mandatory_fouls: mandatory_fouls,
             visitor_fouls: visitor_fouls,
             mandatory_yellow_cards: mandatory_yellow_cards,
             visitor_yellow_cards: visitor_yellow_cards)
    end

    subject { hint.hint_type_team(type, team) }

    context 'when type is fouls' do
      let(:type) { :fouls }

      context 'and team is mandatory' do
        let(:team) { :mandatory }

        it 'return hint mandatory_fouls' do
          is_expected.to eq(hint.mandatory_fouls)
        end
      end

      context 'and team is visitor' do
        let(:team) { :visitor }

        it 'return hint visitor_fouls' do
          is_expected.to eq(hint.visitor_fouls)
        end
      end
    end

    context 'when type is yellow_cards' do
      let(:type) { :yellow_cards }

      context 'and team is mandatory' do
        let(:team) { :mandatory }

        it 'return hint mandatory_yellow_cards' do
          is_expected.to eq(hint.mandatory_yellow_cards)
        end
      end

      context 'and team is visitor' do
        let(:team) { :visitor }

        it 'return hint visitor_yellow_cards' do
          is_expected.to eq(hint.visitor_yellow_cards)
        end
      end
    end
  end

  describe '.match_type_team' do
    let(:mandatory_fouls) { 5 }
    let(:visitor_fouls) { 10 }

    let(:mandatory_yellow_cards) { 10 }
    let(:visitor_yellow_cards) { 5 }

    let(:match) do
      create(:match,
             mandatory_fouls: mandatory_fouls,
             visitor_fouls: visitor_fouls,
             mandatory_yellow_cards: mandatory_yellow_cards,
             visitor_yellow_cards: visitor_yellow_cards)
    end

    let!(:hint) { create(:hint, match: match) }

    subject { hint.match_type_team(type, team) }

    context 'when type is fouls' do
      let(:type) { :fouls }

      context 'and team is mandatory' do
        let(:team) { :mandatory }

        it 'return hint mandatory_fouls' do
          is_expected.to eq(hint.match_mandatory_fouls)
        end
      end

      context 'and team is visitor' do
        let(:team) { :visitor }

        it 'return hint visitor_fouls' do
          is_expected.to eq(hint.match_visitor_fouls)
        end
      end
    end

    context 'when type is yellow_cards' do
      let(:type) { :yellow_cards }

      context 'and team is mandatory' do
        let(:team) { :mandatory }

        it 'return hint mandatory_yellow_cards' do
          is_expected.to eq(hint.match_mandatory_yellow_cards)
        end
      end

      context 'and team is visitor' do
        let(:team) { :visitor }

        it 'return hint visitor_yellow_cards' do
          is_expected.to eq(hint.match_visitor_yellow_cards)
        end
      end
    end
  end
end
