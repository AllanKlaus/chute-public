# frozen_string_literal: true

require 'rails_helper'

describe User do
  let!(:user) { create(:user) }

  describe '.hints' do
    let!(:hint) { create(:hint, user: user) }

    subject { user.hints }

    it 'return hints' do
      is_expected.to include(hint)
    end
  end

  describe '.hints_count' do
    let(:number_of_hints) { 3 }

    subject { user.hints_count }

    before { create_list(:hint, number_of_hints, user: user) }

    it 'return number of hints' do
      is_expected.to eq number_of_hints
    end
  end

  describe '#user_datum' do
    let!(:user_data) { create(:user_datum, user: user) }

    subject { user.user_datum }

    it 'return user_data' do
      is_expected.to eq user_data
    end
  end
end
