#!/bin/bash

USERNAME=klaus
# HOSTS="191.252.193.247 another_ip another_ip"
HOSTS="191.252.193.247"
SCRIPT="pwd; ls" # COMMAND TO EXECUTE DEPLOY
for HOSTNAME in ${HOSTS} ; do
    ssh -l ${USERNAME} ${HOSTNAME} "${SCRIPT}"
done